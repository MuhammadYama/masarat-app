import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SearchPlatePage } from '../search-plate/search-plate';
import { FormBuilder, FormGroup,Validators } from '@angular/forms';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { ClientDetailPage } from '../client-detail/client-detail';
import { Client } from '../../models/client';
import {AlertController} from 'ionic-angular';
import { ProfilePage } from '../profile/profile';
/**
 * Generated class for the RegisterClientPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register-client',
  templateUrl: 'register-client.html',
})
export class RegisterClientPage {
  credentialsForm: FormGroup;
  constructor(public navCtrl: NavController, public navParams: NavParams 
    ,private formBuilder: FormBuilder
    ,private  apiService:ApiServiceProvider
    ,private alertCtrl: AlertController) {

      this.credentialsForm = this.formBuilder.group({
        name: ['', Validators.required],
        phone: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(10)]],
        sex: ['', Validators.required],
      });

  }
  nextPage() {
    this.navCtrl.setRoot(SearchPlatePage);
  }
  back() {
    this.navCtrl.pop()
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterClientPage');
  }

  private addClient() {
      let postdata={
        name: this.credentialsForm.controls.name.value,
        phone: this.credentialsForm.controls.phone.value,
        sex: this.credentialsForm.controls.sex.value,
        currency_code:'ARS',
        enabled:1,
        email:this.credentialsForm.controls.phone.value+'d@dd.cf',
        company_id:1 
      };
          this.apiService.postRequest('customers',postdata)
            .subscribe(
              res => {
                console.log('addClient result '+JSON.stringify(res));
               let client:Client =res['data'];
                this.navCtrl.push(ClientDetailPage, {
                  client: client
                });/*.then(() => {
                  let index = 1;
                  this.navCtrl.remove(index);
                });*/
                  
                //  this.car_types  = res['data'];
  
            },
              error => {     console.log('addClient error '+error );
              this.apiService.getRequest('client?phone='+this.credentialsForm.controls.phone.value+'&company_id=1')
            .subscribe(
              res => {  
                let client:Client =res['data']; 
                this.navCtrl.pop().then(() => {
                
                  this.navCtrl.push(ClientDetailPage, {
                    client: client
                  });
                });
               },
              error => { 

     
                let alert = this.alertCtrl.create({
                  title: 'خطأ',
                      subTitle: 'فشلت العملية  ',
                      buttons: ['إغلاق']
             });
             alert.present();

              });
             
         
            });
  
      
          }



          home(){

            this.navCtrl.popToRoot();
          } 

}
