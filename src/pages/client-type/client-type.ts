import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { NewBillPage } from '../new-bill/new-bill';
import { SearchPlatePage } from '../search-plate/search-plate';
import { ProfilePage } from '../profile/profile';
/**
 * Generated class for the ClientTypePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-client-type',
  templateUrl: 'client-type.html',
})
export class ClientTypePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
  back() {
    this.navCtrl.pop()
  } 
 companyClient() {
    this.navCtrl.push(SearchPlatePage);
  } 
  singleClient() {
    this.navCtrl.push(NewBillPage,{
      type:'client'
      
    });
  }
  ionViewDidLoad() {
   
    console.log('ionViewDidLoad ClientTypePage');
  }



  home(){
    this.navCtrl.popToRoot();
    //this.navCtrl.setRoot(ProfilePage);
  }
}
