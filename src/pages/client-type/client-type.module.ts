import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ClientTypePage } from './client-type';

@NgModule({
  declarations: [
    ClientTypePage,
  ],
  imports: [
    IonicPageModule.forChild(ClientTypePage),
  ],
})
export class ClientTypePageModule {}
