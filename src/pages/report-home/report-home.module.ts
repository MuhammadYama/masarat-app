import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReportHomePage } from './report-home';

@NgModule({
  declarations: [
    ReportHomePage,
  ],
  imports: [
    IonicPageModule.forChild(ReportHomePage),
  ],
})
export class ReportHomePageModule {}
