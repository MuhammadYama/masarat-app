import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { NewReportPage } from '../new-report/new-report';
import { MyReportsPage } from '../my-reports/my-reports';
import { ProfilePage } from '../profile/profile';

/**
 * Generated class for the ReportHomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-report-home',
  templateUrl: 'report-home.html',
}) 
export class ReportHomePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReportHomePage');
  }

 
  back() {
    this.navCtrl.pop()
  } 
 newReport() {
    this.navCtrl.push(NewReportPage);
  } 

  myReports() {
    this.navCtrl.push(MyReportsPage);

}



home(){

  this.navCtrl.popToRoot();
}

}
