import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { StorageServiceProvider } from '../../providers/storage-service/storage-service';
import { AuthenticatedUser } from '../../models/user';
import { PackagesPage } from '../packages/packages';
import { ClientTypePage } from '../client-type/client-type';
import { NewBillPage } from '../new-bill/new-bill';
import { ReportHomePage } from '../report-home/report-home';
import { DailyReportPage } from '../daily-report/daily-report';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { LoginPage } from '../login/login';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { MyInvoicesPage } from '../my-invoices/my-invoices';
import { ReservationsPage } from '../reservations/reservations';


@IonicPage() 
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html', 
})
export class ProfilePage {
  private user: AuthenticatedUser;
  branch_id:any;
  branches:any;
  constructor(public navCtrl: NavController, public navParams: NavParams
    , private userService: StorageServiceProvider
    ,private  apiService:ApiServiceProvider
    , private authService: AuthServiceProvider) {
      if(this.navParams.get('user')){
      this.user= this.navParams.get('user'); 
      }
      else{
        this.user=this.navCtrl['rootParams']['user'];
        this.branch_id  =this.navCtrl['rootParams']['branch_id']
      }


  }
  nextPage() {
    
  } 
  newBill() {
    this.navCtrl.push(ClientTypePage);
  } 
  reportHome() {
    this.navCtrl.push(ReportHomePage);
  } 
  dailyReprt() {
    this.navCtrl.push(DailyReportPage);
  }   
  sellPackage(){
    this.navCtrl.push(NewBillPage,{
      type:'package'
      
    })
   // this.navCtrl.push(PackagesPage);
  } 
  ionViewDidLoad() {
    
    console.log('ionViewDidLoad ProfilePage');
 
   // this.getAuthUser(); 
    
  }
  ionViewCanEnter(){
    this.getBranch();
    this.getBranches();
    //this.getAuthUser(); 
  }
  setBranch( ){

    this.userService.createOnStorage(Number(this.branch_id) ,'branch_id').then(() => {
  
    });

  }

 getBranch(){
  this.userService.getOnStorage('branch_id').then(
    (branch_id) => {

      if (branch_id){
     //   alert(JSON.stringify(branch_id) );
        console.log('ggetClients from storage'+JSON.stringify(branch_id) );
      this.branch_id  = branch_id;
      } /*else
    this.branch_id =this.user.branch['data']['id'];*/

  

    }
  );

    
  }
  private getAuthUser() {
    this.userService.getOnStorage().then(
      (user) => {
    
        if (!user){
             console.log('getAuthUser null '+JSON.stringify(user) );
          //this.navCtrl.push(LoginPage)
         // this.navCtrl.setRoot(LoginPage)
        } else {
          console.log('getAuthUser1 '+JSON.stringify(user) );
      this.user = JSON.parse(user);
    
        
        }
    
      }
    );
    }
   
    logout(){
      this.authService.logout().then(
        (user) => {  this.navCtrl.setRoot(LoginPage); }).catch(
          (err) => { }
          );
    }



    private getBranches() {
      this.userService.getOnStorage('branches').then(
        (branches) => {
     
          if (branches){
            console.log('packages from storage'+JSON.stringify(branches) );
          this.branches  = JSON.parse(branches);
        }
     
            this.apiService.getRequest('branches?company_id=1')
              .subscribe(
                res => {
                  console.log('ggetClients result '+JSON.stringify(res));
                    this.branches  = res['data'];
                this.userService.createOnStorage(res['data'],'branches').then(() => {
     
                });
              },
                error => {     console.log('branches error '+error );});
    
        }
      );
      }

      private myInvoice(){

        this.navCtrl.push(MyInvoicesPage); 
      } 


      reservations(){
        this.navCtrl.push(ReservationsPage);

      } 

    }
  