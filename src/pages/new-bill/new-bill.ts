import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ClientDetailPage } from '../client-detail/client-detail';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { StorageServiceProvider } from '../../providers/storage-service/storage-service';
import { Client } from '../../models/client';
import { ClientTypePage } from '../client-type/client-type';
import { RegisterClientPage } from '../register-client/register-client';
import { AddServicesPage } from '../add-services/add-services';
import { PackagesPage } from '../packages/packages';
import { ProfilePage } from '../profile/profile';
@IonicPage()
@Component({
  selector: 'page-new-bill',
  templateUrl: 'new-bill.html',
})
export class NewBillPage {
  clients: Array<Client>;

  constructor(public navCtrl: NavController, public navParams: NavParams
    , private storageSerivce: StorageServiceProvider
    ,private  apiService:ApiServiceProvider) {
      this.clients =[];
      
  }
  back() {
    this.navCtrl.pop()
  } 

  ionViewDidLoad() {
    console.log('ionViewDidLoad NewBillPage');
  }

  openClientPage(client:Client=null){
    //openServicesPage(client:Client){
  // That's right, we're pushing to ourselves!
    this.navCtrl.push(ClientDetailPage, {
      client: client
    });

  } 

  private getClients() {
    this.storageSerivce.getOnStorage('clients').then(
      (clients) => {
   
        if (clients){
          console.log('ggetClients from storage'+JSON.stringify(clients) );
        this.clients  = JSON.parse(clients);
      }
   
          this.apiService.getRequest('client/normal?company_id=1')
            .subscribe(
              res => {
                console.log('ggetClients result '+JSON.stringify(res));
                  this.clients  = res['data'];
              this.storageSerivce.createOnStorage(res['data'],'clients').then(() => {
   
              });
            },
              error => {     console.log('ggetClients error '+error );});
  
      }
    );
    }


   

    addNewClient(){
      this.navCtrl.push(RegisterClientPage, {
      
      });
      
    } 
    openServicesPage(client:Client){
      if(this.navParams.get('type')=='package')
      this.navCtrl.push(PackagesPage, {
        client:client
        
      }); else 
      this.navCtrl.push(AddServicesPage, {
        client:client
        
      });
    }  
    
    ionViewCanEnter(){
  
      this.getClients();
    }
  
    home(){

      this.navCtrl.popToRoot();
    }
}
