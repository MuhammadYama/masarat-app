import { Component, ViewChild } from '@angular/core';
import { ModalController , IonicPage, NavController, NavParams } from 'ionic-angular';
import { BillDetails2Page } from '../bill-details2/bill-details2';
declare var sunmiInnerPrinter: any;
import { ProfilePage } from '../profile/profile';
@IonicPage()
@Component({
  selector: 'page-bill-dtails',
  templateUrl: 'bill-dtails.html',
})
export class BillDtailsPage {
  @ViewChild('imageCanvas') canvas: any;
  client:any;
  car: any;
  basket_items1:any;
  basket_items2:any;
  items_total:any;
  car_class:any;
  invoiced_at: String = new Date().toISOString();
  canvasElement: any;
  img: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, private modal: ModalController) {
    this.client=this.navParams.get('client');
    this.car=this.navParams.get('car');
    this.car_class=this.navParams.get('car_class');

    this.basket_items1=this.navParams.get('basket_items').filter(it => it.class.data.id!=null);
    
    this.basket_items2=this.navParams.get('basket_items').filter(it => it.class.data.id==null);

    this.setTotal();
 
  }
  setTotal(){
    this.items_total= 0;
          
    
    for (let i = 0; i < this.navParams.get('basket_items').length; i++) {
      this.items_total += this.navParams.get('basket_items')[i].total;
    }
  
   
   

  }
  back() {
    this.navCtrl.pop()
  }
  openModal() {
    const myModal = this.modal.create('ModalPage');

    myModal.present();
  }
  nextPage() {
    //this.print2();
    this.navCtrl.push(BillDetails2Page, {
      client: this.client,
      car: this.car,
      items_total : this.items_total ,
     basket_items:this.navParams.get('basket_items'),
     invoiced_at:this.invoiced_at
    });
  }

  ionViewDidLoad() {
    this.img = new Image(); 
    this.img.src = '../../assets/images/logoo.png';  // Create new img element
    this.img.onload = function() {}
    
   
    console.log('ionViewDidLoad BillDtailsPage');
  }
  print2(){
     //  sunmiInnerPrinter.printOriginalText("Hello Printer");
  
 
      
     this.canvasElement = this.canvas.nativeElement;
     let context = this.canvasElement.getContext('2d'); 
   
     context.fillStyle = "white";
     context.fillRect(0, 0, this.canvasElement.width, this.canvasElement.height);
     context.fillStyle = "black";
     context.drawImage(this.img,0,10,512,152);
     var vspace=220;
    
     context.font = "bold 25px Arial";//"35px Arial";
     context.textAlign="center";
     let label= ' العوالي- بجوار الراجحي مباشرة 0125303118 ';
     context.fillText(label,250,vspace);
     context.font = "bold 35px Arial";
      label= 'فاتورة المبيعات';
     context.fillText(label,250,vspace+105);
     context.font = "bold 35px Arial";
     label=this.invoiced_at+'';
     context.fillText(label,250,vspace+150);
     context.textAlign="end";
     vspace=vspace+85;
     label='الصنف';
     context.fillText(label,500,vspace+130);
     label='السعر';
     context.fillText(label,300,vspace+130);
     label='الكمية';
     context.fillText(label,200,vspace+130);
     label='الإجمالي';
     context.fillText(label,100,vspace+130);
     context.setLineDash([10, 5]);/*dashes are 10px and spaces are 5px*/
     context.beginPath();
     context.moveTo(10,vspace+140);
     context.lineTo(512, vspace+140);
     context.stroke();
     let forv=50;
     for (let i = 0; i < this.navParams.get('basket_items').length; i++) {
       forv=50*(i+1)+130;
       label=this.navParams.get('basket_items')[i].name;
       context.fillText(label,500,vspace+forv);
       label= this.navParams.get('basket_items')[i].sale_price;
       context.fillText(label,300,vspace+forv);
       label=this.navParams.get('basket_items')[i].quantity;
       context.fillText(label,200,vspace+forv);
       label= this.navParams.get('basket_items')[i].total;
       context.fillText(label,100,vspace+forv);
 
     }
     vspace=vspace+forv+20;
     context.setLineDash([10, 5]);/*dashes are 10px and spaces are 5px*/
     context.beginPath();
     context.moveTo(10,vspace);
     context.lineTo(512, vspace);
     context.stroke();
     label= '  الإجمالي : '+this.items_total;
     context.fillText(label,500,vspace+50);
//'image/jpeg'
    // console.log(this.canvasElement.toDataURL("image/png")+'  testbit');
    let image = this.canvasElement.toDataURL('image/jpeg').replace('data:image/jpeg;base64,', '');
  //  let image =context.getImageData(0, 0, 512, vspace+forv+30+50).data;
    sunmiInnerPrinter.printBitmap(image,350,900); 

   // sunmiInnerPrinter.printOriginalText("Hello Printer");

  }

  home(){
    this.navCtrl.popToRoot();
  }



}
