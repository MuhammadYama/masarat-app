import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BillDtailsPage } from './bill-dtails';

@NgModule({
  declarations: [
    BillDtailsPage,
  ],
  imports: [
    IonicPageModule.forChild(BillDtailsPage),
  ],
})
export class BillDtailsPageModule {}
