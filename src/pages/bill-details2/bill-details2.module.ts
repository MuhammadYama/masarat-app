import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BillDetails2Page } from './bill-details2';

@NgModule({
  declarations: [
    BillDetails2Page,
  ],
  imports: [
    IonicPageModule.forChild(BillDetails2Page),
  ],
})
export class BillDetails2PageModule {}
