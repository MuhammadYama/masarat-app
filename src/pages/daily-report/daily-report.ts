import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MyReportsPage } from '../my-reports/my-reports';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { StorageServiceProvider } from '../../providers/storage-service/storage-service';
import { AuthenticatedUser } from '../../models/user';
import {AlertController} from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { ProfilePage } from '../profile/profile';
declare var sunmiInnerPrinter: any;

/**
 * Generated class for the DailyReportPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-daily-report',
  templateUrl: 'daily-report.html',
})
export class DailyReportPage {
  @ViewChild('imageCanvas') canvas: any;
  //Imagedata:any;
  totals: Object;
  date: String = new Date().toISOString();
  selected_date: any;
  canvasElement: any;
  img: any;
  added_by:any;
  prenter_ready:any=false;
  private user: AuthenticatedUser;
  constructor(public navCtrl: NavController, public navParams: NavParams
    , private storageSerivce: StorageServiceProvider 
     ,private alertCtrl: AlertController
     , private authService: AuthServiceProvider
    ,private  apiService:ApiServiceProvider) {
      this.date=this.date.substring(0, 10);
  }
  back() {
    this.navCtrl.pop()
  } 


   onSuccess(data:any) {
    let alertf = this.alertCtrl.create({
      title: data.type,
          subTitle:  data.action,
          buttons: ['إغلاق']
 });
 alertf.present();
  }
  onError(data:any) {
    let alertf = this.alertCtrl.create({
      title: 'خطأ',
          subTitle:  data,
          buttons: ['إغلاق']
 });
 alertf.present();
  }
  ionViewDidLoad() {
 sunmiInnerPrinter.printerStatusStartListener(this.onSuccess,this.onError);


  sunmiInnerPrinter.hasPrinter().then(
      (resolve) => {
        if(resolve==1)
        this.prenter_ready=true;
      }).catch(
        (reject) => {
          let alertf = this.alertCtrl.create({
            title: 'خطأ',
                subTitle:  reject,
                buttons: ['إغلاق']
       });
       alertf.present();
        });
     //   sunmiInnerPrinter.printerInit();
 /*   sunmiInnerPrinter.printerInit().then(
      (resolve) => {

        let alertf = this.alertCtrl.create({
          title: 'success',
              subTitle: 'printerInit'+ resolve,
              buttons: ['إغلاق']
     });
     alertf.present();


      }).catch(
        (reject) => {
          let alertf = this.alertCtrl.create({
            title: 'خطأ',
                subTitle: 'printerInit'+ reject,
                buttons: ['إغلاق']
       });
       alertf.present();
        });*/
   /* sunmiInnerPrinter.printerSelfChecking().then(
      (resolve) => {
        let alertf = this.alertCtrl.create({
          title: 'success',
              subTitle: 'printerSelfChecking'+ resolve,
              buttons: ['إغلاق']
     });
     alertf.present();

      }).catch(
        (reject) => {
          
          let alertf = this.alertCtrl.create({
            title: 'خطأ',
                subTitle: 'printerSelfChecking'+ reject,
                buttons: ['إغلاق']
       });
       alertf.present();


        });*/
    this.getAuthUser(); 
    this.img = new Image(); 
    this.img.src = '../../assets/images/logoo.png';  // Create new img element

    this.img.onload = function() {
  

  //   sunmiInnerPrinter.printBitmap(img,512,200); 
   }
    this.getDaily();
    console.log('ionViewDidLoad DailyReportPage');
  }
  
  private getDaily() {
    this.storageSerivce.getOnStorage('totals').then(
      (totals) => {
   
        if (totals){
          console.log('ggetClients from storage'+JSON.stringify(totals) );
      this.totals  = JSON.parse(totals);
      }
    
          this.apiService.getRequest('get_daily?company_id=1&date='+this.date)
            .subscribe(
              res => {
                console.log('ggetClients result '+JSON.stringify(res));
                  this.totals  = res['data'];
                  this.added_by=res['data']['added_by']
                //  this.printDaily()
              this.storageSerivce.createOnStorage(res['data'],'totals').then(() => {
   
              });
            },
              error => {     console.log('totals error '+error );});
  
      }
    );
    }
    dateChanged(){
    /*  console.log('dateChanged'+this.selected_date);
      this.date = this.selected_date.toISOString();
      this.date=this.date.substring(0, 10);*/
      this.getDaily();
    }



    printDaily2(){
      //  sunmiInnerPrinter.printOriginalText("Hello Printer");
     /* sunmiInnerPrinter.printerInit().then(
        (resolve) => {*/
          this.printDaily2()
         /* let alertf = this.alertCtrl.create({
            title: 'success',
                subTitle: 'printerInit'+ resolve,
                buttons: ['إغلاق']
       });
       alertf.present();
  
  */
       /* }).catch(
          (reject) => {
            let alertf = this.alertCtrl.create({
              title: 'خطأ',
                  subTitle: 'printerInit'+ reject,
                  buttons: ['إغلاق']
         });
         alertf.present();
          });
  */
       
    
 
   }
   printDaily(){
    this.toDataUrl(function(image,vspace) {
      // Do whatever you'd like with the Data URI!

  sunmiInnerPrinter.printBitmap(image,350,vspace).then(
   (resolve) => {
     this.prenter_ready=true;
     let alert = this.alertCtrl.create({
       title: 'تأكيد',
           subTitle:  'تم إرسال أمر الطباعة ',
           buttons: [       {
             text: '      موافق  ',
             role: 'cancel',
             handler: () => {
              this.navCtrl.popToRoot();
             }
           }]
  });
  alert.present();
 
 
   }).catch(
     (reject) => {
       this.prenter_ready=true;
       let alertf = this.alertCtrl.create({
         title: 'خطأ',
             subTitle:  reject,
             buttons: ['إغلاق']
    });
    alertf.present();
 
     }); 
    });
 
  
 //sunmiInnerPrinter.lineWrap(3);
 //sunmiInnerPrinter.exitPrinterBuffer(true);

   }
    toDataUrl( callback) {
    this.prenter_ready=false;
    this.canvasElement = this.canvas.nativeElement;
    let context = this.canvasElement.getContext('2d'); 
  
    context.fillStyle = "white";
    context.fillRect(0, 0, this.canvasElement.width, this.canvasElement.height);
    context.fillStyle = "black";
    context.drawImage(this.img,0,10,512,152);
    var vspace=220;
   
    context.font = "bold 25px Arial";//"35px Arial";
    context.textAlign="end";
    let label= 'الموظف';//'الرقم الضريبي : 000000000000';
    context.fillText(label,500,vspace);
    context.textAlign="start";
    label=''+this.added_by;
    context.fillText(label,0,vspace);
    context.textAlign="end";
    label='المبيعات بتاريخ';//'الرقم الضريبي : 000000000000';
    context.fillText(label,500,vspace+50);
    context.textAlign="start";
    label=''+this.date;
    context.fillText(label,0,vspace+50);
    context.beginPath();
    context.moveTo(10,vspace+70);
    context.lineTo(512, vspace+70);
    context.stroke();
    context.textAlign="end";
    label='مسحوب من الرصيد ';//'الرقم الضريبي : 000000000000';
    context.fillText(label,500,vspace+ 120);
    label='مضاف للرصيد';//'الرقم الضريبي : 000000000000';
    context.fillText(label,500,vspace+ 170);
    label='باقات';//'الرقم الضريبي : 000000000000';
    context.fillText(label,500,vspace+ 220);
    label='دفع مرة واحدة';//'الرقم الضريبي : 000000000000';
    context.fillText(label,500,vspace+ 270);
    
    context.textAlign="start";
    label=' ريال ';//'الرقم الضريبي : 000000000000';
    context.fillText(label,0,vspace+ 120);
    label=this.totals['minus_balance_totals'];//'الرقم الضريبي : 000000000000';
    context.fillText(label,50,vspace+ 120);
    label=' ريال ';//'الرقم الضريبي : 000000000000';
    context.fillText(label,0,vspace+ 170);
    label=this.totals['add_balance_totals'];//'الرقم الضريبي : 000000000000';
    context.fillText(label,50,vspace+ 170);
    label=' ريال ';//'الرقم الضريبي : 000000000000';
    context.fillText(label,0,vspace+ 220);
    label=this.totals['package_total'];//'الرقم الضريبي : 000000000000';
    context.fillText(label,50,vspace+ 220);
    label=' ريال ';//'الرقم الضريبي : 000000000000';
    context.fillText(label,0,vspace+ 270);
    label=''+(this.totals['paid']- this.totals['package_total']);//'الرقم الضريبي : 000000000000';
    context.fillText(label,50,vspace+ 270);
    context.beginPath();
    context.moveTo(10,vspace+270+20);
    context.lineTo(512, vspace+270+20);
    context.stroke();

    context.textAlign="end";
    label='الإجمالي النقدي';//'الرقم الضريبي : 000000000000';
    context.fillText(label,500,vspace+ 340);
    context.textAlign="start";
    label=' ريال ';//'الرقم الضريبي : 000000000000';
    context.fillText(label,0,vspace+ 340);
    label=this.totals['paid'];//'الرقم الضريبي : 000000000000';
    context.fillText(label,50,vspace+ 340);
//'image/jpeg'
   // console.log(this.canvasElement.toDataURL("image/png")+'  testbit');
   let image = this.canvasElement.toDataURL('image/jpeg').replace('data:image/jpeg;base64,', '')
  //  let image =context.getImageData(0, 0, 512, vspace+forv+30+50).data;
 // let image = this.canvasElement.toDataURL('image/jpeg')
 vspace=vspace+ 340 +100;
 //this.Imagedata=image;
 // image = image.replace('data:image/jpeg;base64,', '')

 // sunmiInnerPrinter.setAlignment(1);
 
setTimeout(function () {
  callback(image,vspace)},2000);

   }
   private getAuthUser() {
    this.storageSerivce.getOnStorage().then(
      (user) => {
    
        if (!user){
             console.log('getAuthUser null '+JSON.stringify(user) );
          //this.navCtrl.push(LoginPage)
         // this.navCtrl.setRoot(LoginPage)
        } else {
          console.log('getAuthUser1 '+JSON.stringify(user) );
      this.user = JSON.parse(user);
      
        
        }
     
      }
    );
    }



    checkUser(){
if( this.user){
  for (let i = 0; i < this.user['roles']['data'].length; i++) {
    if(this.user['roles']['data'][i]['code']=='manager'){
    return true;
    }
    
}
return  false;

    }
    return  false;
}

presentPrompt() {
  let alert = this.alertCtrl.create({
    title: 'التحقق من المستخدم',
    inputs: [
      {
        name: 'username',
        placeholder: 'اسم المستخدم'
      },
      {
        name: 'password',
        placeholder: 'كلمة المرور',
        type: 'password'
      }
    ],
    buttons: [
      {
        text: 'إلغاء',
        role: 'cancel',
        handler: data => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'تحقق',
        handler: data => {

          console.log('تحقق clicked');
          this.checkUserPrint(data.username,data.password);
         /* if (User.isValid(data.username, data.password)) {
            // logged in!
          } else {
            // invalid login
            return false;
          }*/
        }
      }
    ]
  });
  alert.present();
}



checkUserPrint(username:any,password:any){
   

 // Your app login API web service call triggers
 this.authService.adminCheck(username, password).then(
(user) => {
  let x=false;
     if(user){
      for (let i = 0; i < user['roles']['data'].length; i++) {
        if(user['roles']['data'][i]['code']=='manager'){
          x=true;
          //this.printDaily();
        }
        
    }

    
        }
        if(x)
        this.printDaily();
        else {
        let alert = this.alertCtrl.create({
          title: 'خطأ',
          subTitle: 'ليس لديك الصلاحية للطباعة ',
          buttons: ['إغلاق']
 });
     alert.present();
        }

}).catch(
  (err) => {
    //show the error
    if(err=='incorrect'){
    let alertf = this.alertCtrl.create({
      title: 'خطأ',
          subTitle: 'خطأ في كلمة المرور او اسم المستخدم',
          buttons: ['إغلاق']
 });
 alertf.present();
}
        console.log('login error'+err);
  }
);
//    this.navCtrl.setRoot(ListPage);
}


home(){

  this.navCtrl.popToRoot();
}


}
