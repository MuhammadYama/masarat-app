import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {AlertController} from 'ionic-angular';
import { CompanyBillPage } from '../company-bill/company-bill';
import { ProfilePage } from '../profile/profile';
/**
 * Generated class for the CompanyCarPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */ 

@IonicPage()
@Component({
  selector: 'page-company-car',
  templateUrl: 'company-car.html',
})

export class CompanyCarPage {
  company_client:Object;
  enabled:boolean=true;;
  constructor(public navCtrl: NavController, public navParams: NavParams
    ,private alertCtrl: AlertController) {
     
    this.company_client=this.navParams.get('company_client');
    if(this.company_client['wash_no']<=this.company_client['no_washed']) {
      this.enabled=false;
      let alert = this.alertCtrl.create({
        title: 'نأسف',
            subTitle: ' نأسف لقد استفذت جميع الغسلات المسموحه لك ',
            buttons: ['إغلاق']
   });
   alert.present();
    }


    if(!this.company_client['companyInfo']['data']['balance']||this.company_client['companyInfo']['data']['balance']['data']['amount']<=this.company_client['price']){
      this.enabled=false;
      let alert = this.alertCtrl.create({
        title: 'نأسف',
            subTitle: 'نأسف الرصيد غير كافي ',
            buttons: ['إغلاق']
   });
   alert.present();
    }
  }

  ionViewDidLoad() {
  
    console.log('ionViewDidLoad CompanyCarPage');
  }
  back() {
    this.navCtrl.pop()
  }


  billDtails(){
    
    this.navCtrl.push(CompanyBillPage, {
      company_client: this.company_client,
     

    });
  }

  home(){

    this.navCtrl.popToRoot();
  } 
}
