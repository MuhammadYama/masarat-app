import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CompanyCarPage } from './company-car';

@NgModule({
  declarations: [
    CompanyCarPage,
  ],
  imports: [
    IonicPageModule.forChild(CompanyCarPage),
  ],
})
export class CompanyCarPageModule {}
