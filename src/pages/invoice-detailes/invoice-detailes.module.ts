import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InvoiceDetailesPage } from './invoice-detailes';

@NgModule({
  declarations: [
    InvoiceDetailesPage,
  ],
  imports: [
    IonicPageModule.forChild(InvoiceDetailesPage),
  ],
})
export class InvoiceDetailesPageModule {}
