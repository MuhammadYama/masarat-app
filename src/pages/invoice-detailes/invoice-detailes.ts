import { Component ,ViewChild} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ViewController } from 'ionic-angular';
import { AuthenticatedUser } from '../../models/user';
import { StorageServiceProvider } from '../../providers/storage-service/storage-service';
import { ProfilePage } from '../profile/profile';
import {AlertController} from 'ionic-angular';

declare var sunmiInnerPrinter: any;
/**
 * Generated class for the InvoiceDetailesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-invoice-detailes',
  templateUrl: 'invoice-detailes.html',
})
export class InvoiceDetailesPage {
  @ViewChild('imageCanvas') canvas: any;
  invoice:Object;
  invoices: Array<Object>;

 invoice_total:any=0;
 invoice_subtotal:any=0;
 invoice_tax:any=0;
 invoice_number:any=0;
 invoice_datetime:any;
 img: any;
 canvasElement: any;
 private user: AuthenticatedUser;
 prenter_ready:any=false;
  constructor(public navCtrl: NavController       ,private alertCtrl: AlertController, private storageSerivce: StorageServiceProvider, public navParams: NavParams,public viewCtrl : ViewController) {

    this.invoice=this.navParams.get('invoice');
    this.invoices=this.navParams.get('invoices');
  }


  onSuccess(data:any) {
    let alertf = this.alertCtrl.create({
      title: data.type,
          subTitle:  data.action,
          buttons: ['إغلاق']
 });
 alertf.present();
  }
  onError(data:any) {
    let alertf = this.alertCtrl.create({
      title: 'خطأ',
          subTitle:  data,
          buttons: ['إغلاق']
 });
 alertf.present();
  }
  ionViewDidLoad() {
 sunmiInnerPrinter.printerStatusStartListener(this.onSuccess,this.onError);


  sunmiInnerPrinter.hasPrinter().then(
      (resolve) => {
        if(resolve==1)
        this.prenter_ready=true;
      }).catch(
        (reject) => {
          let alertf = this.alertCtrl.create({
            title: 'خطأ',
                subTitle:  reject,
                buttons: ['إغلاق']
       });
       alertf.present();
        });
    this.getAuthUser(); 
    this.img = new Image(); 
    this.img.src = '../../assets/images/logoo.png';  // Create new img element
    this.img.onload = function() {}
    console.log('ionViewDidLoad InvoiceDetailesPage');
  }

  public closeModal(){
    this.viewCtrl.dismiss( this.invoices);
  }




  printInvoice(invoice:object){
    this.prenter_ready=false;
    //  sunmiInnerPrinter.printOriginalText("Hello Printer");
    for (let i = 0; i < invoice['totals']['data'].length; i++) {
      if(invoice['totals']['data'][i]['code']=='sub_total')
      this.invoice_subtotal=invoice['totals']['data'][i]['amount'].toFixed(2);
      else if(invoice['totals']['data'][i]['code']=='tax')
           this.invoice_tax=invoice['totals']['data'][i]['amount'].toFixed(2);
           else if(invoice['totals']['data'][i]['code']=='total')
                 this.invoice_total=invoice['totals']['data'][i]['amount'].toFixed(2);
    }

     
    this.canvasElement = this.canvas.nativeElement;
    let context = this.canvasElement.getContext('2d'); 
  
    context.fillStyle = "white";
    context.fillRect(0, 0, this.canvasElement.width, this.canvasElement.height);
    context.fillStyle = "black";
    context.drawImage(this.img,0,10,512,152);
    var vspace=220;
   
    context.font = "bold 25px Arial";//"35px Arial";
    context.textAlign="center";
    let label= ' العوالي- بجوار الراجحي مباشرة 0125303118 ';
    context.fillText(label,250,vspace);
    label='الرقم الضريبي : 302176768600003';
    context.fillText(label,250,vspace+50);
    context.font = "bold 35px Arial";
     label= invoice['invoice_number']+' : رقم الفاتورة  ';
    context.fillText(label,250,vspace+105);
    context.font = "bold 25px Arial";
    label= invoice['invoiced_at']+'';
    context.fillText(label,250,vspace+150);
    context.textAlign="end";
    vspace=vspace+85;
    label='الصنف';
    context.fillText(label,500,vspace+130);
    label='السعر';
    context.fillText(label,300,vspace+130);
    label='الكمية';
    context.fillText(label,200,vspace+130);
    label='الإجمالي';
    context.fillText(label,100,vspace+130);
    context.setLineDash([10, 5]);/*dashes are 10px and spaces are 5px*/
    context.beginPath();
    context.moveTo(10,vspace+140);
    context.lineTo(512, vspace+140);
    context.stroke();
    let forv=50;
    for (let i = 0; i < invoice['items']['data'].length; i++) {
      forv=50*(i+1)+130;
      label=invoice['items']['data'][i]['name'];
      context.fillText(label,500,vspace+forv);
      label= invoice['items']['data'][i]['price'];
      context.fillText(label,300,vspace+forv);
      label=invoice['items']['data'][i]['quantity'];
      context.fillText(label,200,vspace+forv);
      label=invoice['items']['data'][i]['total']+invoice['items']['data'][i]['tax'];
      context.fillText(label,100,vspace+forv);

    }
    vspace=vspace+forv+20;
    context.setLineDash([10, 5]);/*dashes are 10px and spaces are 5px*/
    context.beginPath();
    context.moveTo(10,vspace);
    context.lineTo(512, vspace);
    context.stroke();
    label= 'Subtoatal / المجموع';
    context.fillText(label,500,vspace+50);
    label='VAT (5%) /ضريبة القيمة المضافة';
    context.fillText(label,500,vspace+50+70);
    label= 'Total  /الإجمالي';
    context.fillText(label,500,vspace+50+70+70);
    

    context.textAlign="start";
    label='ريال';
    context.fillText(label,0,vspace+50);
    label= this.invoice_subtotal;
    context.fillText(label,50,vspace+50);
    label='ريال';
    context.fillText(label,0,vspace+50+70);
    label= this.invoice_tax;
    context.fillText(label,50,vspace+50+70); 
    label='ريال';
    context.fillText(label,0,vspace+50+70+70);
    label=this.invoice_total;
    context.fillText(label,50,vspace+50+70+70);
    vspace=vspace+50+70+70+100;
//'image/jpeg'
   // console.log(this.canvasElement.toDataURL("image/png")+'  testbit');
   let image = this.canvasElement.toDataURL('image/jpeg').replace('data:image/jpeg;base64,', '');
 //  let image =context.getImageData(0, 0, 512, vspace+forv+30+50).data;
   sunmiInnerPrinter.printBitmap(image,350,vspace).then(
    (resolve) => {
      this.prenter_ready=true;
      let alert = this.alertCtrl.create({
        title: 'تأكيد',
            subTitle:  'تم إرسال أمر الطباعة ',
            buttons: [       {
              text: '      موافق  ',
              role: 'cancel'/* ,
             handler: () => {
                this.navCtrl.popToRoot();
              }*/
            }]
   });
   alert.present();
  
  
    }).catch(
      (reject) => {
        this.prenter_ready=true;
        let alertf = this.alertCtrl.create({
          title: 'خطأ',
              subTitle:  reject,
              buttons: ['إغلاق']
     });
     alertf.present();
  
      });  

  // sunmiInnerPrinter.printOriginalText("Hello Printer");

 }



 private getAuthUser() {
  this.storageSerivce.getOnStorage().then(
    (user) => {
  
      if (!user){
           console.log('getAuthUser null '+JSON.stringify(user) );
        //this.navCtrl.push(LoginPage)
       // this.navCtrl.setRoot(LoginPage)
      } else {
        console.log('getAuthUser1 '+JSON.stringify(user) );
    this.user = JSON.parse(user);
    
      
      }
  
    }
  );
  }



  checkUser(){
if( this.user){
for (let i = 0; i < this.user['roles']['data'].length; i++) {
  if(this.user['roles']['data'][i]['code']=='manager'){
  return true;
  }
  
}
return  false;

  }
  return  false;
}

home(){

  this.navCtrl.popToRoot();
}


}
  