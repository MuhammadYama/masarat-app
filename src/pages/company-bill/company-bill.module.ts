import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CompanyBillPage } from './company-bill';

@NgModule({
  declarations: [
    CompanyBillPage,
  ],
  imports: [
    IonicPageModule.forChild(CompanyBillPage),
  ],
})
export class CompanyBillPageModule {}
