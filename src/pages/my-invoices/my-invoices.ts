import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { StorageServiceProvider } from '../../providers/storage-service/storage-service';
import {AlertController} from 'ionic-angular';
import { ModalController } from 'ionic-angular';
import { InvoiceDetailesPage } from '../invoice-detailes/invoice-detailes';
import { ProfilePage } from '../profile/profile';
/**
 * Generated class for the MyReportsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-my-invoices',
  templateUrl: 'my-invoices.html',
})
export class MyInvoicesPage {
  invoices: Array<Object>;
  constructor(public navCtrl: NavController, public navParams: NavParams
    , private storageSerivce: StorageServiceProvider
    ,private  apiService:ApiServiceProvider
    ,private alertCtrl: AlertController
    , private userService: StorageServiceProvider
    ,private modalCtrl: ModalController) {

  }


  ionViewDidLoad() {
    this.getMyInvoices();
    console.log('ionViewDidLoad MyReportsPage');
  }
 
  back() {
    this.navCtrl.pop()
  } 


  private getMyInvoices() {
    this.storageSerivce.getOnStorage('mInvoices').then(
      (invoices) => {
   
        if (invoices){
          console.log('my_invoice from storage'+JSON.stringify(invoices) );
        this.invoices  = JSON.parse(invoices);
      }
   
          this.apiService.getRequest('my_invoice?company_id=1')
            .subscribe(
              res => {
                console.log('invoices result '+JSON.stringify(res));
                  this.invoices  = res['data'];
              this.storageSerivce.createOnStorage(res['data'],'reinvoicesports').then(() => {
   
              });
            },
              error => {     console.log('invoices error '+error );});
  
      }
    );
    }
    getStatusColor(status:any){
     if(status==1)
     return 'danger';
     else 
     if(status==2)
     return 'primary';
     else 
     return 'light';

    }  
 
    details(invoice:Object){

      var modalPage = this.modalCtrl.create(InvoiceDetailesPage,{ invoice : invoice,invoices : this.invoices });
      modalPage.onDidDismiss(data => {
        this.invoices=data;
   });
      modalPage.present();
    }


    home(){

      this.navCtrl.popToRoot();
    }
  
} 

 