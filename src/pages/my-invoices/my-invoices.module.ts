import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyInvoicesPage } from './my-invoices';

@NgModule({
  declarations: [
    MyInvoicesPage,
  ],
  imports: [
    IonicPageModule.forChild(MyInvoicesPage),
  ],
})
export class MyInvoicesPageModule {}
