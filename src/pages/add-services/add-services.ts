import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BillDtailsPage } from '../bill-dtails/bill-dtails';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { StorageServiceProvider } from '../../providers/storage-service/storage-service';
import { BasketItem } from '../../models/basket_item';
import {AlertController} from 'ionic-angular';
import { Client } from '../../models/client';
import { AuthenticatedUser } from '../../models/user';
import { AddServices2Page } from '../add-services2/add-services2';
//import { Service } from '../../models/service';
/**
 * Generated class for the AddServicesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-services',
  templateUrl: 'add-services.html', 
}) 
export class AddServicesPage {
  services: Array<object>;
  items: Array<object>;
  basket_items: Array<BasketItem>=[];
  client:Client;
  client_cars:Array<Object>;
  car_classes:Array<Object>;
  car_types:Array<Object>;
  class_items:Array<Object>;
  car_class:string;
  packages:Array<Object>;
  car:any;
  car_type_index:number;
  private user: AuthenticatedUser;
  branch_id:any;
  constructor(public navCtrl: NavController, public navParams: NavParams
    , private storageSerivce: StorageServiceProvider
    ,private  apiService:ApiServiceProvider
    ,private alertCtrl: AlertController
    , private userService: StorageServiceProvider) {
      this.client=this.navParams.get('client');
      if(this.client)
      this.client_cars=this.client['cars']['data'];
  }
  nextPage() {
    this.navCtrl.push(AddServices2Page, {
      client: this.client,
      car: this.car,
      car_class: this.car_class,
     
      
    });
  }
  back() {
    this.navCtrl.pop() 
  }
  ionViewDidLoad() {
    this.getBranch();
    console.log('ionViewDidLoad AddServicesPage');
  }

  backServices(){
    this.items=null;
  }
  private getServices() {
    this.storageSerivce.getOnStorage('services').then(
      (services) => {
  
        if (services){
          console.log('ggetClients from storage'+JSON.stringify(services) );
        this.services  = JSON.parse(services);
      }
  
          this.apiService.getRequest('services?company_id=1')
            .subscribe(
              res => {
                console.log('ggetservicesresult '+JSON.stringify(res));
                  this.services  = res['data'];
              this.storageSerivce.createOnStorage(res['data'],'services').then(() => {
  
              });
            },
              error => {     console.log('ggetservices error '+error );});
  
      }
    );
    } 

    openServiceMenu(service:object){

    /*  let alert = this.alertCtrl.create({
        title: 'خطأ',
            subTitle: 'خطأ في كلمة المرور او اسم المستخدم',
            buttons: ['إغلاق']
   });
   alert.present();*/
   
      //this.items=service['items']['data'];
      this.items=service['items']['data'].filter(it => it.class.data.id==null ||it.class.data.name.includes(this.car_class));
      
    }
emptyStorageAlert(){

  let alert = this.alertCtrl.create({
    title: 'نفذت الكمية',
        subTitle: 'لايوجد كمية كافية في المخزن ',
        buttons: ['إغلاق']
});
alert.present();
}
    addtoBasket(item:BasketItem){
      
      if(this.basket_items.some((oitem) => oitem['id'] == item['id'])) {
        if(!this.checkClass(item)){
          let infor=false;
          if(item['storages']&&item['storages']['data'].length>1){
             infor=false;
          for(let i=0;item['storages']['data'].length>i; i++ ){
           if(item['storages']['data'][i]['branch_id']==this.branch_id){
            infor=true;
           if(item['storages']['data'][i]['quantity']>item.quantity)
         {
          item.quantity++;
          item.total=item.quantity*item['sale_price'];
         }else this.emptyStorageAlert()
        }
          } if(!infor)
          this.emptyStorageAlert()
          } else this.emptyStorageAlert()
      
      }
      } else {
        if(!this.checkClass(item)){
          let infor=false;
          if(item['storages']&&item['storages']['data'].length>1){
             infor=false;
          for(let i=0;item['storages']['data'].length>i; i++ ){
           if(item['storages']['data'][i]['branch_id']==this.branch_id){
            infor=true;
           if(item['storages']['data'][i]['quantity']>1)
         {
          console.log('addtoBasket add to basket');
          item.quantity=1;
          item.price=item['sale_price'];
          item.currency="ARS";
          item.item_id=item['id']
          item.total=item.quantity*item['sale_price'];
          this.basket_items.push(item);
         } else this.emptyStorageAlert()
          }
        } if(!infor)
        this.emptyStorageAlert()
          }else this.emptyStorageAlert()
          }else {

            console.log('addtoBasket add to basket');
            item.quantity=1;
            item.price=item['sale_price'];
            item.currency="ARS";
            item.item_id=item['id']
            item.total=item.quantity*item['sale_price'];
            this.basket_items.push(item);
          }

      }
    }

    increseQuantity(item:BasketItem ){
      if(!this.checkClass(item)){
        if(item['storages']){
          let infor=false;
        for(let i=0;item['storages']['data'].length>i; i++ ){
         if(item['storages']['data'][i]['branch_id']==this.branch_id){
          infor=true;
         if(item['storages']['data'][i]['quantity']>item.quantity)
       {
      item.quantity++;
      item.total=item.quantity*item['sale_price']
       } else this.emptyStorageAlert()
      }
      } if(!infor)
      this.emptyStorageAlert()
    }else this.emptyStorageAlert()
    }
    }
 

    private getCarClasses() {
      this.storageSerivce.getOnStorage('car_classes').then(
        (car_classes) => {
     
          if (car_classes){
            console.log('ggetClients from storage'+JSON.stringify(car_classes) );
          this.car_classes  = JSON.parse(car_classes);
        }
     
            this.apiService.getRequest('car_classes?company_id=1')
              .subscribe(
                res => {
                  console.log('car_classes result '+JSON.stringify(res));
                    this.car_classes  = res['data'];
                this.storageSerivce.createOnStorage(res['data'],'car_classes').then(() => {
     
                });
              },
                error => {     console.log('car_classes error '+error );});
    
        }
      );
      }

      
    private getCarTypes() {
        this.storageSerivce.getOnStorage('car_types').then(
          (car_types) => {
       
            if (car_types){
              console.log('ggetClients from storage'+JSON.stringify(car_types) );
            this.car_types  = JSON.parse(car_types);
          }
       
              this.apiService.getRequest('car_types?company_id=1')
                .subscribe(
                  res => {
                    console.log('car_types result '+JSON.stringify(res));
                      this.car_types  = res['data'];
                  this.storageSerivce.createOnStorage(res['data'],'car_types').then(() => {
       
                  });
                },
                  error => {     console.log('car_types error '+error );});
      
          }
        );
        }
 

   private getClassItems() {
          this.storageSerivce.getOnStorage('class_items').then(
            (class_items) => {
         
              if (class_items){
                console.log('ggetClients from storage'+JSON.stringify(class_items) );
              this.class_items  = JSON.parse(class_items);
            }
         
                this.apiService.getRequest('class_items?company_id=1')
                  .subscribe(
                    res => {
                      console.log('class_items result '+JSON.stringify(res));
                        this.class_items  = res['data'];
                    this.storageSerivce.createOnStorage(res['data'],'class_items').then(() => {
         
                    });
                  },
                    error => {     console.log('class_items error '+error );});
        
            }
          );
          }
    decreeseQuantity(index ,item:BasketItem){
      
      if(item.quantity==1){
        this.basket_items.splice(index, 1);     
      }else if(!this.checkClass(item)){
        item.quantity--;
        item.total=item.quantity*item['sale_price']}
      
    }
    ionViewCanEnter(){
  
      this.getServices();
      this.getCarClasses();
      this.getCarTypes();
      this.getClassItems();
      this.getPackages();
      this.getAuthUser(); 
    }
    private checkClass(item){ 
  if(item.class&&item.class.data.id==null)
  return false;
  else 
  return true;

}



private getPackages() {
  this.storageSerivce.getOnStorage('packages').then(
    (packages) => {
 
      if (packages){
        console.log('packages from storage'+JSON.stringify(packages) );
      this.packages  = JSON.parse(packages);
    }
 
        this.apiService.getRequest('package?company_id=1')
          .subscribe(
            res => {
              console.log('ggetClients result '+JSON.stringify(res));
                this.packages  = res['data'];
            this.storageSerivce.createOnStorage(res['data'],'packages').then(() => {
 
            });
          },
            error => {     console.log('packages error '+error );});

    }
  );
  }

  billDtails(){
    this.navCtrl.push(BillDtailsPage, {
      client: this.client,
      car: this.car,
      car_class: this.car_class,
      basket_items:this.basket_items
      
    });
  }




  private addInvoice() {
    if(this.client){
     let customer_id= this.client.id;
     let customer_name =this.client.name;
    }else {
      let customer_id= null;
      let customer_name =null;
    }
    let postdata={
      "company_id" : 1,
     // "invoice_number": "INV-00003",
      "invoice_status_code": "paid",
      "invoiced_at": "2019-08-01 09:12:12",//
      "due_at": "2019-08-02 09:12:12",//
      "amount": 0,
      "item" :this.basket_items ,
      "currency_code": "ARS",
      "currency_rate": 3.75,
      "customer_id": this.client.id,
      "customer_name":this.client.name,
      "category_id": 3,
      "branch_id":this.user.branch.id
  };
//JSON.stringify(postdata)
  console.log('addClient result '+JSON.stringify(postdata));
        this.apiService.postRequest('invoice_add',postdata)
          .subscribe(
            res => {
              console.log('addClient result '+JSON.stringify(postdata));
             let client:Client =res['data'];
            /*  this.navCtrl.push(ClientDetailPage, {
                client: client
              });*//*.then(() => {
                let index = 1;
                this.navCtrl.remove(index);
              });*/
                
              //  this.car_types  = res['data'];

         },
            error => {    /* console.log('addClient error '+error );
            this.apiService.getRequest('client?phone='+this.credentialsForm.controls.phone.value+'&company_id=1')
          .subscribe(
            res => {  
              let client:Client =res['data']; 
              this.navCtrl.pop().then(() => {
              
                this.navCtrl.push(ClientDetailPage, {
                  client: client
                });
              });
             },
            error => { 
*/
   
              let alert = this.alertCtrl.create({
                title: 'خطأ',
                    subTitle: 'فشلت العملية  ',
                    buttons: ['إغلاق']
           });
           alert.present();

            });
           
       
        //  });

   
        }



        private getAuthUser() {
          this.userService.getOnStorage().then(
            (user) => {
          
              if (!user){
                   console.log('getAuthUser null '+JSON.stringify(user) );
                //this.navCtrl.push(LoginPage)
               // this.navCtrl.setRoot(LoginPage)
              } else {
                console.log('getAuthUser1 '+JSON.stringify(user) );
            this.user = JSON.parse(user);
          
              
              }
          
            }
          );
          }


          onCarChange(){
            if(this.client)
            this.car_class=this.client_cars[this.car]['carType']['data']['class']['data']['name'];
            else
            this.car_class=this.car_types[this.car_type_index]['class']['data']['name'];
            console.log('his.car_type_index '+this.car_class);
            this.items=null;
          }




          getBranch(){
            this.userService.getOnStorage('branch_id').then(
              (branch_id) => {
          
                if (branch_id){
               //   alert(JSON.stringify(branch_id) );
                  console.log('ggetClients from storage'+JSON.stringify(branch_id) );
                this.branch_id  = branch_id;
                } /*else
              this.branch_id =this.user.branch['data']['id'];*/
          
            
          
              }
            );
          
              
            }


            home(){
              //this.navCtrl.setRoot(ProfilePage);
              this.navCtrl.popToRoot(); 
            //  this.navCtrl.setRoot(ProfilePage,{ user: this.user ,branch_id:this.branch_id});
            }
      
}
