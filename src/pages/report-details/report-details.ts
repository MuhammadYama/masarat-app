import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ViewController } from 'ionic-angular';
import {AlertController} from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { StorageServiceProvider } from '../../providers/storage-service/storage-service';
import { ProfilePage } from '../profile/profile';
/**
 * Generated class for the ReportDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-report-details',
  templateUrl: 'report-details.html',
})
export class ReportDetailsPage {
report:Object;
reports:Array<Object>;
  constructor(public navCtrl: NavController, public navParams: NavParams
    ,public viewCtrl : ViewController
    , private storageSerivce: StorageServiceProvider
    ,private  apiService:ApiServiceProvider
    ,private alertCtrl: AlertController
    , private userService: StorageServiceProvider) {
      this.report=this.navParams.get('report');
      this.reports=this.navParams.get('reports');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReportDetailsPage');
  }
  public closeModal(){
    this.viewCtrl.dismiss( this.reports);
  }
  confirmClose() {
  let alert = this.alertCtrl.create({
    title: '   تنبيه ',
        subTitle: '   هل انت متأكد من إغلاق البلاغ؟ ',
         buttons: [
          {
            text: '  إلغاء',
            role: 'cancel',
            handler: () => {
              console.log('إلغاء ');
            }
          },
          {
            text: ' نعم ',
            handler: () => {
              this.closeReport();
            }
          }
        ]
});

alert.present();
}
  closeReport(){
    
    let postdata={
      report_id: this.report['id'],
      company_id:1 
    };
        this.apiService.postRequest('close_report',postdata)
          .subscribe(
            res => {
              console.log('addClient result '+JSON.stringify(res));
              this.reports=res['data']; 
              this.closeModal();
              /*.then(() => {
                let index = 1;
                this.navCtrl.remove(index);
              });*/
                
              //  this.car_types  = res['data'];

          },
            error => {     console.log('addClient error '+error );
           /* this.apiService.getRequest('client?phone='+this.credentialsForm.controls.phone.value+'&company_id=1')
          .subscribe(
            res => {  
              let client:Client =res['data']; 
              this.navCtrl.pop().then(() => {
              
                this.navCtrl.push(ClientDetailPage, {
                  client: client
                });
              });
             },
            error => { */

   
              let alert = this.alertCtrl.create({
                title: 'خطأ',
                    subTitle: 'فشلت العملية  ',
                    buttons: ['إغلاق']
           });
           alert.present();

            });
           
       

  }


  home(){

    this.navCtrl.popToRoot();
  }



}
