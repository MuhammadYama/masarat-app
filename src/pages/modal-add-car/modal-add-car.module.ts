import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalAddCarPage } from './modal-add-car';

@NgModule({
  declarations: [
    ModalAddCarPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalAddCarPage),
  ],
})
export class ModalAddCarPageModule {}
