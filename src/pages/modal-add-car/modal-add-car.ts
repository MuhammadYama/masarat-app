import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ViewController } from 'ionic-angular';
import { FormBuilder, FormGroup,Validators } from '@angular/forms';
import { StorageServiceProvider } from '../../providers/storage-service/storage-service';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { Client } from '../../models/client';
import {AlertController} from 'ionic-angular';
/**
 * Generated class for the ModalAddCarPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modal-add-car',
  templateUrl: 'modal-add-car.html',
})
export class ModalAddCarPage {

  credentialsForm: FormGroup;
  car_types:Array<Object>;
   client:Client;
   cars:Array<Object>;
  constructor(public navCtrl: NavController, public navParams: NavParams
    ,public viewCtrl : ViewController
    ,private formBuilder: FormBuilder
    , private storageSerivce: StorageServiceProvider
    ,private  apiService:ApiServiceProvider
    ,private alertCtrl: AlertController) {
      this.client=this.navParams.get('client');
      this.cars=this.client['cars']['data'];
      this.credentialsForm = this.formBuilder.group({
        name: ['', Validators.required],
        number: ['', [Validators.required, Validators.minLength(6)]],
        type: ['', Validators.required],
      });
  }

  ionViewDidLoad() {
    this.getCarTypes();
    console.log('ionViewDidLoad ModalAddCarPage');
  }
  public closeModal(){
    this.viewCtrl.dismiss(this.cars);
  }



  private getCarTypes() {
    this.storageSerivce.getOnStorage('car_types').then(
      (car_types) => {
   
        if (car_types){
          console.log('ggetClients from storage'+JSON.stringify(car_types) );
        this.car_types  = JSON.parse(car_types);
      }
   
          this.apiService.getRequest('car_types?company_id=1')
            .subscribe(
              res => {
                console.log('car_types result '+JSON.stringify(res));
                  this.car_types  = res['data'];
              this.storageSerivce.createOnStorage(res['data'],'car_types').then(() => {
   
              });
            },
              error => {     console.log('car_types error '+error );});
  
      }
    );
    }


    private addCar() {
      let postdata={
        name: this.credentialsForm.controls.name.value,
        plate_no: this.credentialsForm.controls.number.value,
        car_type_id: this.credentialsForm.controls.type.value,
        customer_id:this.client.id,

        company_id:1 
      };
          this.apiService.postRequest('client/car',postdata)
            .subscribe(
              res => {
                console.log('addClient result '+JSON.stringify(res));
                this.cars=res['data']; 
                this.closeModal();
                /*.then(() => {
                  let index = 1;
                  this.navCtrl.remove(index);
                });*/
                  
                //  this.car_types  = res['data'];
  
            },
              error => {     console.log('addClient error '+error );
             /* this.apiService.getRequest('client?phone='+this.credentialsForm.controls.phone.value+'&company_id=1')
            .subscribe(
              res => {  
                let client:Client =res['data']; 
                this.navCtrl.pop().then(() => {
                
                  this.navCtrl.push(ClientDetailPage, {
                    client: client
                  });
                });
               },
              error => { */

     
                let alert = this.alertCtrl.create({
                  title: 'خطأ',
                      subTitle: 'فشلت العملية  ',
                      buttons: ['إغلاق']
             });
             alert.present();

              });
             
         
            //});
  
      
          }

}
