import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { StorageServiceProvider } from '../../providers/storage-service/storage-service';
import {AlertController} from 'ionic-angular';
import { ModalController } from 'ionic-angular';
import { InvoiceDetailesPage } from '../invoice-detailes/invoice-detailes';
import { ProfilePage } from '../profile/profile';
/**
 * Generated class for the ReservationsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-reservations',
  templateUrl: 'reservations.html',
})
export class ReservationsPage {
  reservations: Array<Object>;
  constructor(public navCtrl: NavController, public navParams: NavParams
    , private storageSerivce: StorageServiceProvider
    ,private  apiService:ApiServiceProvider
    ,private alertCtrl: AlertController
    , private userService: StorageServiceProvider
    ,private modalCtrl: ModalController) {

  }


  ionViewDidLoad() {
    this.getMyInvoices();
    console.log('ionViewDidLoad MyReportsPage');
  }
 
  back() {
    this.navCtrl.pop()
  } 


  private getMyInvoices() {
    this.storageSerivce.getOnStorage('reservations').then(
      (reservations) => {
   
        if (reservations){
          console.log('my_invoice from storage'+JSON.stringify(reservations) );
        this.reservations  = JSON.parse(reservations);
      }
   
          this.apiService.getRequest('reservations?company_id=1')
            .subscribe(
              res => {
                console.log('reservations result '+JSON.stringify(res));
                  this.reservations  = res['data'];
              this.storageSerivce.createOnStorage(res['data'],'reservations').then(() => {
   
              });
            },
              error => {     console.log('reservations error '+error );});
  
      }
    );
    }
    getStatusColor(status:any){
     if(status==1)
     return 'danger';
     else 
     if(status==2)
     return 'primary';
     else 
     return 'light';

    }  
 
    details(invoice:Object){

    /*  var modalPage = this.modalCtrl.create(InvoiceDetailesPage,{ invoice : invoice,invoices : this.invoices });
      modalPage.onDidDismiss(data => {
        this.reservations=data;
   });
      modalPage.present();*/
    }


    home(){

      this.navCtrl.popToRoot();
    }
  
} 

 