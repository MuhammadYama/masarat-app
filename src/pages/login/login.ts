import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ProfilePage } from '../profile/profile';
import { FormBuilder, FormGroup,Validators } from '@angular/forms';
import {AlertController} from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { StorageServiceProvider } from '../../providers/storage-service/storage-service';
import { AuthenticatedUser } from '../../models/user';
declare var sunmiInnerPrinter: any;
@IonicPage()

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  credentialsForm: FormGroup;
  private user: AuthenticatedUser;
  constructor(public navCtrl: NavController, public navParams: NavParams
    ,private alertCtrl: AlertController
    ,private formBuilder: FormBuilder
   , private authService: AuthServiceProvider
    , private userSerivce: StorageServiceProvider
    ) {
      this.credentialsForm = this.formBuilder.group({
        username: ['', Validators.required],
        password: ['', Validators.required]
      });
      
  }

  print(){
    sunmiInnerPrinter.printOriginalText("Hello Printer");
    }
    

  login(){
   
    console.log('pass:'+ this.credentialsForm.controls.password.value );
    console.log('username:'+ this.credentialsForm.controls.username.value );
   // Your app login API web service call triggers
   this.authService.login(this.credentialsForm.controls.username.value, this.credentialsForm.controls.password.value ).then(
  (user) => {
       console.log('authnticted user  '+user.name );

       this.userSerivce.createOnStorage(user.branch['data']['id'],'branch_id').then(() => {
        this.userSerivce.createOnStorage(user,'user').then(() => {
      
          this.navCtrl.setRoot(ProfilePage,{
            user: user});
          });
      });


  }).catch(
    (err) => {
      //show the error
      let alert = this.alertCtrl.create({
        title: 'خطأ',
            subTitle: 'خطأ في كلمة المرور او اسم المستخدم',
            buttons: ['إغلاق']
   });
   alert.present();
   this.credentialsForm.controls.username.setValue('');
   this.credentialsForm.controls.password.setValue('');
          console.log('login error'+err);
    }
  );
//    this.navCtrl.setRoot(ListPage);
 }

  nextPage() {
    this.navCtrl.setRoot(ProfilePage);
  }

  ionViewDidLoad() {
   // sunmiInnerPrinter.printOriginalText("Hello Printer");

   // print();
    console.log('ionViewDidLoad LoginPage');
  }

}
 