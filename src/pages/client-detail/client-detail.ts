import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AddServicesPage } from '../add-services/add-services';
import { Client } from '../../models/client';
import { ModalController } from 'ionic-angular';
import { ModalAddCarPage } from '../modal-add-car/modal-add-car';
import { ProfilePage } from '../profile/profile';
/**
 * Generated class for the ClientDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-client-detail',
  templateUrl: 'client-detail.html',
})
export class ClientDetailPage {
client:Client;
cars:Array<Object>;

  constructor(public navCtrl: NavController, public navParams: NavParams
    ,private modalCtrl: ModalController) {
    this.client=this.navParams.get('client');

    this.cars=this.client['cars']['data'];
  }
  nextPage() {
    this.navCtrl.setRoot(AddServicesPage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ClientDetailPage');
  }
  back() {
    this.navCtrl.pop()
  }


  public opeAddCarModal(){
  
    var modalPage = this.modalCtrl.create(ModalAddCarPage,{ client : this.client });
    modalPage.onDidDismiss(data => {
      this.cars=data;
 });
    modalPage.present();
  }


  home(){
    this.navCtrl.popToRoot();
   // this.navCtrl.setRoot(ProfilePage);
  }


}
