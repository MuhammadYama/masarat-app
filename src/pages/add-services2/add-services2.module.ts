import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddServices2Page } from './add-services2';

@NgModule({
  declarations: [
    AddServices2Page,
  ],
  imports: [
    IonicPageModule.forChild(AddServices2Page),
  ],
})
export class AddServices2PageModule {}
