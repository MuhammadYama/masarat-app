import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { StorageServiceProvider } from '../../providers/storage-service/storage-service';
import {AlertController} from 'ionic-angular';
import { ModalController } from 'ionic-angular';
import { ReportDetailsPage } from '../report-details/report-details';
import { ProfilePage } from '../profile/profile';
/**
 * Generated class for the MyReportsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-my-reports',
  templateUrl: 'my-reports.html',
})
export class MyReportsPage {
  reports: Array<Object>;
  constructor(public navCtrl: NavController, public navParams: NavParams
    , private storageSerivce: StorageServiceProvider
    ,private  apiService:ApiServiceProvider
    ,private alertCtrl: AlertController
    , private userService: StorageServiceProvider
    ,private modalCtrl: ModalController) {
  }


  ionViewDidLoad() {
    this.getMyReports();
    console.log('ionViewDidLoad MyReportsPage');
  }

  back() {
    this.navCtrl.pop()
  } 


  private getMyReports() {
    this.storageSerivce.getOnStorage('reports').then(
      (reports) => {
   
        if (reports){
          console.log('my_reports from storage'+JSON.stringify(reports) );
        this.reports  = JSON.parse(reports);
      }
   
          this.apiService.getRequest('my_reports?company_id=1')
            .subscribe(
              res => {
                console.log('reports result '+JSON.stringify(res));
                  this.reports  = res['data'];
              this.storageSerivce.createOnStorage(res['data'],'reports').then(() => {
   
              });
            },
              error => {     console.log('reports error '+error );});
  
      }
    );
    }
    getStatusColor(status:any){
     if(status==1)
     return 'danger';
     else 
     if(status==2)
     return 'primary';
     else 
     return 'light';

    } 
 
    details(report:Object){

      var modalPage = this.modalCtrl.create(ReportDetailsPage,{ report : report,reports : this.reports });
      modalPage.onDidDismiss(data => {
        this.reports=data;
   });
      modalPage.present();
    }

    home(){

      this.navCtrl.popToRoot();
    }

}
