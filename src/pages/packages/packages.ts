import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { StorageServiceProvider } from '../../providers/storage-service/storage-service';
import { BasketItem } from '../../models/basket_item';
import { Client } from '../../models/client';
import { AuthenticatedUser } from '../../models/user';
import {AlertController} from 'ionic-angular';

import { ProfilePage } from '../profile/profile';
declare var sunmiInnerPrinter: any;
/**
 * Generated class for the PackagesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-packages',
  templateUrl: 'packages.html',
})
export class PackagesPage {
@ViewChild('imageCanvas') canvas: any;
packages:Array<Object>;
basket_items: Array<BasketItem>=[];
client:Client;
invoiced_at: String = new Date().toISOString();
private user: AuthenticatedUser;
invoice_total:any=0;
invoice_subtotal:any=0;
invoice_tax:any=0;
invoice_number:any=0;
invoice_datetime:any;
img: any;

canvasElement: any;
payment_way:any;
branch_id:any;
prenter_ready:any=false;
payment_alert= this.alertCtrl.create({
  title: '   طريقة الدفع ',
      subTitle: ' الرجاء احتيار طريقة الدفع  ',
      inputs : [
        {
        type:'radio',
        label:'كاش',
        value:'كاش',
        handler: () => {
          this.payment_way='كاش';          this.addInvoice();            }

        },
        {
        type:'radio',
        label:'بطاقة',
        value:'بطاقة',
        handler: () => {
          this.payment_way='بطاقة';           this.addInvoice();         }
 
        }
   
        ],
        buttons: [
          {
            text: ' إلغاء ',
            role: 'cancel',
            handler: () => {
              console.log('دفع المبلغ نقدا');
            }
          }
        ]

});
  constructor(public navCtrl: NavController, public navParams: NavParams
    , private storageSerivce: StorageServiceProvider
    ,private  apiService:ApiServiceProvider
    , private userService: StorageServiceProvider
    ,private alertCtrl: AlertController
    ) {
      this.client=this.navParams.get('client');
  }
  nextPage() {
  }

  
  onSuccess(data:any) {
    let alertf = this.alertCtrl.create({
      title: data.type,
          subTitle:  data.action,
          buttons: ['إغلاق']
 });
 alertf.present();
  }
  onError(data:any) {
    let alertf = this.alertCtrl.create({
      title: 'خطأ',
          subTitle:  data,
          buttons: ['إغلاق']
 });
 alertf.present();
  }



  ionViewDidLoad() {
    sunmiInnerPrinter.printerStatusStartListener(this.onSuccess,this.onError);


    sunmiInnerPrinter.hasPrinter().then(
        (resolve) => {
          if(resolve==1)
          this.prenter_ready=true;
        }).catch(
          (reject) => {
            let alertf = this.alertCtrl.create({
              title: 'خطأ',
                  subTitle:  reject,
                  buttons: ['إغلاق']
         });
         alertf.present();
          });
    this.img = new Image(); 
    this.img.src = '../../assets/images/logoo.png';  // Create new img element
    this.img.onload = function() {}
    this.getBranch()
    this.getPackages();
    this.getAuthUser(); 
    console.log('ionViewDidLoad PackagesPage');
  }

  private getPackages() {
    this.storageSerivce.getOnStorage('packages').then(
      (packages) => {
   
        if (packages){
          console.log('packages from storage'+JSON.stringify(packages) );
        this.packages  = JSON.parse(packages);
      }
   
          this.apiService.getRequest('package?company_id=1')
            .subscribe(
              res => {
                console.log('ggetClients result '+JSON.stringify(res));
                  this.packages  = res['data'];
              this.storageSerivce.createOnStorage(res['data'],'packages').then(() => {
   
              });
            },
              error => {     console.log('packages error '+error );});
  
      }
    );
    }
    addtoBasket(item:BasketItem){
      item.quantity=1;
      item.price=item['sale_price'];
      item.currency="ARS";
      item.item_id=item['id']
      item.total=item.quantity*item['sale_price'];
      this.basket_items.push(item);
      let alert = this.alertCtrl.create({
        title: '   تأكيد ',
            subTitle: '   هل انت متأكد من شراء  '+item['name'],
      
             buttons: [
              {
                text: ' إلغاء ',
                role: 'cancel',
                handler: () => {
                  console.log('دفع المبلغ نقدا');
                }
              },
              {
                text: 'موافق ',
                handler: () => {
              //    this.addInvoice();     
              this.payment_alert.present(); 
                          }
              }
            ]
   });
  
   alert.present();
    }



    private addInvoice() {
      let postdata={
        "company_id" : 1,
       // "invoice_number": "INV-00003",
        "invoice_status_code": "draft",
        "invoiced_at": this.invoiced_at.substring(0, 10)+' '+this.invoiced_at.substring(11, 16)+':00',//
        "due_at": this.invoiced_at.substring(0, 10)+' '+this.invoiced_at.substring(11, 16)+':00',//
        "amount": 0,
        "item" :this.basket_items ,
        "currency_code": "ARS",
        "currency_rate": 3.75,
        "customer_id": this.client.id,
        "customer_name":this.client.name,
        "category_id": 3,
        "paid":this.basket_items[0]['price'],
        "added_by":this.user.id,
        "branch_id":this.branch_id,
        "payment_way":  this.payment_way,
    };
  //JSON.stringify(postdata)
    console.log('addClient result '+JSON.stringify(postdata));
          this.apiService.postRequest('invoice_add',postdata)
            .subscribe(
              res => {
             /*   console.log('addClient result '+JSON.stringify(postdata));
               let client:Client =res['data'];
               this.navCtrl.getPrevious().data.client =client;
               this.navCtrl.pop();*/
      
               this.invoice_number=res['data']['invoice_number'];
               this.invoice_datetime=res['data']['invoiced_at'];
               for (let i = 0; i < res['data']['totals']['data'].length; i++) {
                if(res['data']['totals']['data'][i]['code']=='sub_total')
                this.invoice_subtotal=res['data']['totals']['data'][i]['amount'].toFixed(2);
                else if(res['data']['totals']['data'][i]['code']=='tax')
                     this.invoice_tax=res['data']['totals']['data'][i]['amount'].toFixed(2);
                     else if(res['data']['totals']['data'][i]['code']=='total')
                           this.invoice_total=res['data']['totals']['data'][i]['amount'].toFixed(2);
              }
               this.printInvoice();
              /*   let alert = this.alertCtrl.create({
                  title: 'تأكيد',
                      subTitle:  ' تم حفظ الفاتوره جاري ارسال امر الطباعة ',
                      buttons: [       {
                        text: '      موافق  ',
                        role: 'cancel',
                        handler: () => {
                         this.navCtrl.popToRoot();
                        }
                      }]
             });
             alert.present();*/
           },
              error => {    /* console.log('addClient error '+error );
              this.apiService.getRequest('client?phone='+this.credentialsForm.controls.phone.value+'&company_id=1')
            .subscribe(
              res => {  
                let client:Client =res['data']; 
                this.navCtrl.pop().then(() => {
                
                  this.navCtrl.push(ClientDetailPage, {
                    client: client
                  });
                });
               },
              error => { 
  */
     
                let alert = this.alertCtrl.create({
                  title: 'خطأ',
                      subTitle: 'فشلت العملية  ',
                      buttons: ['إغلاق']
             });
             alert.present();
  
              });
             
         
          //  });
  
     
          }
  

          private getAuthUser() {
            this.userService.getOnStorage().then(
              (user) => {
            
                if (!user){
                     console.log('getAuthUser null '+JSON.stringify(user) );
                  //this.navCtrl.push(LoginPage)
                 // this.navCtrl.setRoot(LoginPage)
                } else {
                  console.log('getAuthUser1 '+JSON.stringify(user) );
              this.user = JSON.parse(user);
            
                
                }
            
              }
            );
            }
            back() {
              this.navCtrl.pop()
            }




            printInvoice(){
              //  sunmiInnerPrinter.printOriginalText("Hello Printer");
           
          
               
              this.canvasElement = this.canvas.nativeElement;
              let context = this.canvasElement.getContext('2d'); 
            
              context.fillStyle = "white";
              context.fillRect(0, 0, this.canvasElement.width, this.canvasElement.height);
              context.fillStyle = "black";
              context.drawImage(this.img,0,10,512,152);
              var vspace=220;
             
              context.font = "bold 25px Arial";//"35px Arial";
              context.textAlign="center";
              let label= ' العوالي- بجوار الراجحي مباشرة 0125303118 ';
              context.fillText(label,250,vspace);
              label='الرقم الضريبي : 302176768600003';
              context.fillText(label,250,vspace+50);
              context.font = "bold 35px Arial";
               label= this.invoice_number+' : رقم الفاتورة  ';
              context.fillText(label,250,vspace+105);
              context.font = "bold 25px Arial";
              label= this.invoice_datetime+'';
              context.fillText(label,250,vspace+150);
              context.textAlign="end";
              vspace=vspace+85;
              label='الصنف';
              context.fillText(label,500,vspace+130);
              label='السعر';
              context.fillText(label,300,vspace+130);
              label='الكمية';
              context.fillText(label,200,vspace+130);
              label='الإجمالي';
              context.fillText(label,100,vspace+130);
              context.setLineDash([10, 5]);/*dashes are 10px and spaces are 5px*/
              context.beginPath();
              context.moveTo(10,vspace+140);
              context.lineTo(512, vspace+140);
              context.stroke();
              let forv=50;
              for (let i = 0; i < this.basket_items.length; i++) {
                forv=50*(i+1)+130;
                label=this.basket_items[i]['name'];
                context.fillText(label,500,vspace+forv);
                label= this.basket_items[i]['sale_price'];
                context.fillText(label,300,vspace+forv);
                label=this.basket_items[i]['quantity']+'';
                context.fillText(label,200,vspace+forv);
                label= this.basket_items[i]['total']+'';
                context.fillText(label,100,vspace+forv);
          
              }
              vspace=vspace+forv+20;
              context.setLineDash([10, 5]);/*dashes are 10px and spaces are 5px*/
              context.beginPath();
              context.moveTo(10,vspace);
              context.lineTo(512, vspace);
              context.stroke();
              label= 'Subtoatal / المجموع';
              context.fillText(label,500,vspace+50);
              label='VAT (5%) /ضريبة القيمة المضافة';
              context.fillText(label,500,vspace+50+70);
              label= 'Total  /الإجمالي';
              context.fillText(label,500,vspace+50+70+70);
              
    
              context.textAlign="start";
              label='ريال';
              context.fillText(label,0,vspace+50);
              label= this.invoice_subtotal;
              context.fillText(label,50,vspace+50);
              label='ريال';
              context.fillText(label,0,vspace+50+70);
              label= this.invoice_tax;
              context.fillText(label,50,vspace+50+70);
              label='ريال';
              context.fillText(label,0,vspace+50+70+70);
              label=this.invoice_total;
              context.fillText(label,50,vspace+50+70+70);
              vspace=vspace+50+70+70+100;
         //'image/jpeg'
             // console.log(this.canvasElement.toDataURL("image/png")+'  testbit');
             let image = this.canvasElement.toDataURL('image/jpeg').replace('data:image/jpeg;base64,', '');
           //  let image =context.getImageData(0, 0, 512, vspace+forv+30+50).data;
             sunmiInnerPrinter.printBitmap(image,350,vspace).then(
              (resolve) => {
    
                let alert = this.alertCtrl.create({
                  title: 'تأكيد',
                      subTitle:  ' تم حفظ الفاتوره و ارسال امر الطباعة ',
                      buttons: [       {
                        text: '      موافق  ',
                        role: 'cancel',
                        handler: () => {
                          this.navCtrl.popToRoot();
                        }
                      }]
             });
             alert.present();
            
            
              }).catch(
                (reject) => {
                  let alertf = this.alertCtrl.create({
                    title: 'خطأ',
                        subTitle:  'تم حفظ الفاتوره   خطأ في الطباعة ',
                        buttons: ['إغلاق']
               });
               alertf.present();
            
                });  
         
            // sunmiInnerPrinter.printOriginalText("Hello Printer");
         
           }
    
           getBranch(){
            this.userService.getOnStorage('branch_id').then(
              (branch_id) => {
          
                if (branch_id){
               //   alert(JSON.stringify(branch_id) );
                  console.log('ggetClients from storage'+JSON.stringify(branch_id) );
                this.branch_id  = branch_id;
                } /*else
              this.branch_id =this.user.branch['data']['id'];*/
          
            
          
              }
            );
          
              
            }





            home(){

              this.navCtrl.popToRoot();
            }
}
