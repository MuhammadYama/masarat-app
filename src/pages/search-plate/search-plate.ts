import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { NewReportPage } from '../new-report/new-report';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { StorageServiceProvider } from '../../providers/storage-service/storage-service';
import { CompanyCarPage } from '../company-car/company-car';
import { AddServicesPage } from '../add-services/add-services';

/**
 * Generated class for the SearchPlatePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-search-plate',
  templateUrl: 'search-plate.html',
})
export class SearchPlatePage {
  searchTerm: string ;
  filterItems:any;
  client_cars: Array<Object>;
  constructor(public navCtrl: NavController, public navParams: NavParams
    , private storageSerivce: StorageServiceProvider
    ,private  apiService:ApiServiceProvider) {
      this.client_cars =[];
  }
 

  ionViewDidLoad() {
    console.log('ionViewDidLoad SearchPlatePage');
  }

  back() {
    this.navCtrl.pop()
  }  

  private getClientCars() { 
    this.storageSerivce.getOnStorage('clients').then(
      (client_cars) => {
   
        if (client_cars){
          console.log('client_cars from storage'+JSON.stringify(client_cars) );
        this.client_cars  = JSON.parse(client_cars);
      }
   
          this.apiService.getRequest('client/car?company_id=1')
            .subscribe(
              res => {
                console.log('client_cars result '+JSON.stringify(res));
                  this.client_cars  = res['data'];
              this.storageSerivce.createOnStorage(res['data'],'client_cars').then(() => {
   
              });
            },
              error => {     console.log('client_cars error '+error );});
  
      }
    ); 
    }

    searchCar(){}
    ionViewCanEnter(){
  
      this.getClientCars(); 
    } 


    openServicesPage(client:Object,){

    
    }  


  openClientPage(client:Object=null){
    //openServicesPage(client:Client){
  // That's right, we're pushing to ourselves!
    this.navCtrl.push(CompanyCarPage, {
      company_client: client
    });

  } 


 

}
