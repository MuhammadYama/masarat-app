import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DailyReportPage } from '../daily-report/daily-report';
import { FormBuilder, FormGroup,Validators } from '@angular/forms';
import { StorageServiceProvider } from '../../providers/storage-service/storage-service';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import {AlertController} from 'ionic-angular';
import { ProfilePage } from '../profile/profile';
/**
 * Generated class for the NewReportPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-new-report',
  templateUrl: 'new-report.html',
})
export class NewReportPage {
  credentialsForm: FormGroup;
  branches:any;
  types:any;
  branch_id:any;
  constructor(public navCtrl: NavController, public navParams: NavParams
    ,private formBuilder: FormBuilder
    , private storageSerivce: StorageServiceProvider
    ,private  apiService:ApiServiceProvider
    ,private alertCtrl: AlertController) {
      this.credentialsForm = this.formBuilder.group({
        type: ['', Validators.required],
        location: ['', Validators.required],
        desc: ['', Validators.required],
      });

  }
  nextPage() {
    this.navCtrl.setRoot(DailyReportPage);
  }

  ionViewDidLoad() {
    this.getBranches();
    this.getTypes();
    this.getBranch();
    console.log('ionViewDidLoad NewReportPage');
  }
  back() {
    this.navCtrl.pop()
  } 
  confirmAddReport() {
    let alert = this.alertCtrl.create({
      title: '   تنبيه ',
          subTitle: '   هل انت متأكد من رفع البلاغ؟ ',
           buttons: [
            {
              text: '  إلغاء',
              role: 'cancel',
              handler: () => {
                console.log('إلغاء ');
              }
            },
            {
              text: ' نعم ',
              handler: () => {
                this.addReport();
              }
            }
          ]
  });
  
  alert.present();
  } 
  private addReport() {
    let postdata={
      type: this.credentialsForm.controls.type.value,
      desc: this.credentialsForm.controls.desc.value,
      location: this.credentialsForm.controls.location.value,
  

      company_id:1 
    };
        this.apiService.postRequest('add_report',postdata)
          .subscribe(
            res => {
              console.log('addClient result '+JSON.stringify(res));
             // this.reports=res['data']; 
             this.navCtrl.pop()
              /*.then(() => {
                let index = 1;
                this.navCtrl.remove(index);
              });*/
                
              //  this.car_types  = res['data'];

          },
            error => {     console.log('addClient error '+error );
           /* this.apiService.getRequest('client?phone='+this.credentialsForm.controls.phone.value+'&company_id=1')
          .subscribe(
            res => {  
              let client:Client =res['data']; 
              this.navCtrl.pop().then(() => {
              
                this.navCtrl.push(ClientDetailPage, {
                  client: client
                });
              });
             },
            error => { */

   
              let alert = this.alertCtrl.create({
                title: 'خطأ',
                    subTitle: 'فشلت العملية  ',
                    buttons: ['إغلاق']
           });
           alert.present();

            });
           
       
          //});

    
        }





        private getBranches() {
          this.storageSerivce.getOnStorage('branches').then(
            (branches) => {
         
              if (branches){
                console.log('packages from storage'+JSON.stringify(branches) );
              this.branches  = JSON.parse(branches);
            }
         
                this.apiService.getRequest('branches?company_id=1')
                  .subscribe(
                    res => {
                      console.log('ggetClients result '+JSON.stringify(res));
                        this.branches  = res['data'];
                    this.storageSerivce.createOnStorage(res['data'],'branches').then(() => {
         
                    });
                  },
                    error => {     console.log('branches error '+error );});
        
            }
          );
          }

          private getTypes() {
            this.storageSerivce.getOnStorage('report_types').then(
              (report_types) => {
           
                if (report_types){
                  console.log('packages from storage'+JSON.stringify(report_types) );
                this.types  = JSON.parse(report_types);
              }
           
                  this.apiService.getRequest('report_types?company_id=1')
                    .subscribe(
                      res => {
                        console.log('ggetClients result '+JSON.stringify(res));
                          this.types  = res['data'];
                      this.storageSerivce.createOnStorage(res['data'],'report_types').then(() => {
           
                      });
                    },
                      error => {     console.log('branches error '+error );});
          
              }
            );
            }

            getBranch(){
              this.storageSerivce.getOnStorage('branch_id').then(
                (branch_id) => {
            
                  if (branch_id){
                 //   alert(JSON.stringify(branch_id) );
                    console.log('ggetClients from storage'+JSON.stringify(branch_id) );
                  this.branch_id  = branch_id;
                  } /*else
                this.branch_id =this.user.branch['data']['id'];*/
            
              
            
                }
              );
            
                
              }


              home(){

                this.navCtrl.popToRoot();
              }
}
