import { HttpClient,HttpHeaders} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { map, catchError } from 'rxjs/operators';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import {AlertController} from 'ionic-angular';
 

/*
  Generated class for the ApiServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ApiServiceProvider {

 

  private baseUrl = "http://msarat-cw.sa/api/";   
   //private baseUrl = "http://masarat.esnaad.tk/api/";   
  //private baseUrl = "http://masarat.test/api/";  
  private cmopany_id = "?company_id=1";  
     token: String;
 
  constructor( public http: HttpClient,private alertCtrl: AlertController) {
    console.log('Hello ApiServiceProvider Provider');
 this.token = "";

  }
  setToken(value) {
    this.token = value;
  }

  getToken() {
    return this.token;
  }



    /**
    * Get the Json Web Token from the local storage.
    *
    * @returns {RequestOptions}
    */
   private formatHeader(): any {
console.log('formatHeader start ');
if(!this.token||this.token==''){
  return {
              headers: new HttpHeaders({
                'Accept':  'application/json',
                 'Content-Type':  'application/json',
                 'Access-Control-Allow-Origin':  '*',
                  'Access-Control-Allow-Methods':  'GET,POST,OPTIONS,DELETE,PUT'
       })
    };


}
console.log('Authorization Basic ' + this.token);

return  { 
            headers: new HttpHeaders({
              'Accept': 'vnd.api.v1+json',
              'Content-Type':  'application/json',
             //  'Access-Control-Allow-Origin':  '*',
               // 'Access-Control-Allow-Methods':  'GET,POST,OPTIONS,DELETE,PUT',
                 'Authorization': 'Basic ' + this.token
     })
  };

   }

   /**
    * Get the body of an HTTP response.
    *
    * @param res
    * @returns {any|{}}
    */
/*   private handleBody(res: Response) {
     let body = res;
     return body || { };

    // return res|| {};
  }*/
   private handleBody(res: Response) {
     console.log('handleBody '+JSON.stringify(res) );
   let body = res;
   if(body['error']){
   let alert = this.alertCtrl.create({
     title: 'خطأ',
         subTitle: body['error'],
         buttons: ['إغلاق']
});
alert.present();
}
 
   return body || { }; 
 }

   /**
    * Format the error message of an HTTP response.
    *
    * @param error
    * @returns {any}
    */
    private handleError (error: Response | any) {
      console.log('handleError ' +JSON.stringify(error));
      let errMsg: string;
      if (error instanceof Response) {
        const err = error || '';
        errMsg = `${error.status} - ${error['error'] || ''} ${err}`;
      } else {
        errMsg = error.message ? error.message : error.toString();
      }

      console.error(errMsg);
      return Observable.throw(errMsg);
    }
   /* ---------------------------------------------------------------------------------------------------------------- */

   /**
    * Perform a PUT request.
    *
    * @param url
    * @param auth
    * @param body
    * @returns {Observable<>}
    */
   putRequest(url: string, body: Object, auth: boolean = true): Observable<{}> {
     let header = {};

  //   if (auth) {
       header = this.formatHeader();
  //   }
     return this.http.put(this.baseUrl + url, body,header).pipe(
       map(this.handleBody),
       catchError(this.handleError)
     );
   }

   /**
    * Perform a POST request.
    *
    * @param url
    * @param auth
    * @param body
    * @returns {Observable<>}
    */
   postRequest(url: string, body: Object , auth: boolean = true): Observable<Object> {
console.log('postRequest '+this.token );
     let header = {};

  //   if (auth) {
       header = this.formatHeader();
    // }

    /* header = {
              headers: new HttpHeaders({
                  'Accept':  'application/json',
                //   'Content-Type':  'application/json',
                //   'Access-Control-Allow-Origin':  '*',
                  //  'Access-Control-Allow-Methods':  'GET,POST,OPTIONS,DELETE,PUT',
       })
  };*/
     return this.http.post(this.baseUrl + url, body, header).pipe(
       map(this.handleBody),
       catchError(this.handleError)
     );
   }

   /**
    * Perform a HEAD request.
    *
    * @param url
    * @param auth
    * @returns {Observable<>}
    */
   headRequest(url: string, auth: boolean = true): Observable<Object> {
     let header = {};

    // if (auth) {
       header = this.formatHeader();
  //   }
     return this.http.head(this.baseUrl + url, header).pipe(
       map(this.handleBody),
       catchError(this.handleError)
     );
   }

   /**
    * Perform a GET request.
    *
    * @param url
    * @param auth
    * @returns {Promise<>}
    */
   getRequest(url: string, auth: boolean = true) :  Observable<{}> {
     let header = {}

    // if(auth) {
       header = this.formatHeader();
  //   }

     return this.http.get(this.baseUrl + url, header).pipe(
       map(this.handleBody),
       catchError(this.handleError)
     );
   }

   getRequest2(url: string, auth: any) :  Observable<{}> {
    let header = {}

   // if(auth) {
      header =  { 
        headers: new HttpHeaders({
          'Accept': 'vnd.api.v1+json',
          'Content-Type':  'application/json',
         //  'Access-Control-Allow-Origin':  '*',
           // 'Access-Control-Allow-Methods':  'GET,POST,OPTIONS,DELETE,PUT',
             'Authorization': 'Basic ' + auth
 })
};
 //   }

    return this.http.get(this.baseUrl + url, header).pipe(
      map(this.handleBody),
      catchError(this.handleError)
    );
  }




   /**
    * Perform a DELETE request.
    *
    * @param url
    * @param auth
    * @returns {Observable<>}
    */
   deleteRequest(url: string, auth: boolean = true): Observable<Object> {
     let header = {};

  //   if (auth) {
       header = this.formatHeader();
    // }
     return this.http.delete(this.baseUrl + url, header).pipe(
       map(this.handleBody),
       catchError(this.handleError)
     );
   }




}
