import { Injectable } from '@angular/core';
import { AuthenticatedUser } from '../../models/user';
import { Subject } from 'rxjs/Subject';
import { Storage } from '@ionic/storage';


@Injectable()
export class StorageServiceProvider {

  private _user: Subject<any> = new Subject<any>();

  constructor(private storage: Storage) {}

  /* ---------------------------------------------------------------------------------------------------------------- */
  /* Observable use object                                                                                            */

  public subscribeToUserService(callback) {
    return this._user.subscribe(callback);
  }

  public updateUserService(user: any) {
    this._user.next(user);
  }

  /* ---------------------------------------------------------------------------------------------------------------- */
  /* User storage management                                                                                          */

  /**
   * Write user properties in the local storage.
   *
   * @param user
   * @returns {Promise<any>}
   */
  createOnStorage(user: any, key: string ='user' ): Promise<any> {

    return new Promise((resolve) => {
      this.getOnStorage(key).then((res) => {
        if (res) {
          this.storage.remove(key);
        //  this.deleteOnStorage().then(() => {

        //  });
        }
      }).then(() => {
        this.updateUserService(user);
        this.storage.set(key, JSON.stringify(user));
        resolve();
      });
    });
  }

  /**
   * Get user properties from local storage.
   *
   * @returns {Promise<any>}
   */
  getOnStorage( key: string='user'): Promise<any> {

      //this.storage.clear();
    return new Promise((resolve) => {
    this.storage.get(key).then(data => {
          this.updateUserService(JSON.parse(data));

      })
      resolve(this.storage.get(key));
    });
  }

  /**
   * Get user properties from local storage.
   *
   * @returns {Promise<any>}
   */
  getOnStorageSync( key: string='user') {

    this.storage.get(key).then(data => {
        this.updateUserService(JSON.parse(data));

    })

    return this.storage.get(key);
  }

  /**
   * Update user properties from local storage.
   *
   * @param user
   * @returns {Promise<any>}
   */
  updateOnStorage(user: any, key: string='user'): Promise<any> {

    return new Promise((resolve) => {
      resolve(this.storage.get(key));
    });
  }

  /**
   * Delete user properties from local storage.
   *
   * @returns {Promise<any>}
   */
  deleteOnStorage(): Promise<any> {
    return new Promise((resolve) => {
      this.storage.clear();
      resolve();
    });
  }
}
