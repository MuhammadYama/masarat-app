
import { Injectable } from '@angular/core';
import { StorageServiceProvider } from '../storage-service/storage-service';
import { ApiServiceProvider } from '../api-service/api-service';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { AuthenticatedUser } from '../../models/user';
import {  LoadingController } from 'ionic-angular';

/*
  Generated class for the AuthServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthServiceProvider {
 private user: AuthenticatedUser;
    private isLoggedIn = false;
  constructor(private userService: StorageServiceProvider
    , private apiService: ApiServiceProvider
    ,private loadingCtrl: LoadingController) {
    console.log('Hello AuthServiceProvider Provider');
     this.getAuthUser();
  }

  getAuthUser() { 
      this.userService.getOnStorage().then(
         (user) => {
           this.user = user;
         }
      );
    }

    // Login a user
    // Normally make a server request and store
    // e.g. the auth token
    login(email: string, password: string): Promise<AuthenticatedUser>  {
  console.log('Hello AuthServiceProvider Provider login');

  let loading = this.loadingCtrl.create({
    //content: 'Please wait...'
    dismissOnPageChange:true
  });
   loading.present();
   let _authdata = window.btoa(email + ':' + password);
   this.apiService.setToken(_authdata);
      return new Promise((resolve, reject) => {
        this.apiService.getRequest('users/'+email)
          .subscribe(
            res => {
         let u= new  AuthenticatedUser(res['data']);
              u.authdata=_authdata;
              resolve(u) ;    this.apiService.setToken(_authdata); }, 
            error => {  reject(<any>error); loading.dismiss();  this.apiService.setToken('');});

      });
    //  this.isLoggedIn = true;
    }

    // Logout a user, destroy token and remove
    // every information related to a user
    logout(): Promise<any> {
    return new Promise((resolve) => {
      this.userService.deleteOnStorage().then(() => {
        resolve();
      });
    });
  }

    // Returns whether the user is currently authenticated
    // Could check if current token is still valid
    authenticated() : boolean {
      if (this.user.authdata) {
      return true;
    } else {
      return false;
    }
    //  return this.isLoggedIn;
    }


    adminCheck(email: string, password: string): Promise<AuthenticatedUser>  {
      console.log('Hello AuthServiceProvider Provider login');
    
      let loading = this.loadingCtrl.create({
        //content: 'Please wait...'
        dismissOnPageChange:true
      });
       loading.present();
       let _authdata = window.btoa(email + ':' + password);
     //  this.apiService.setToken(_authdata);
          return new Promise((resolve, reject) => {
            this.apiService.getRequest2('users/'+email,_authdata)
              .subscribe(
                res => {
             let u= new  AuthenticatedUser(res['data']);
             loading.dismiss();  
              //    u.authdata=_authdata;
                  resolve(u) ;  
                 //  this.apiService.setToken(_authdata);
                 }, 
                error => { reject('incorrect'); loading.dismiss();   //this.apiService.setToken('');
              });
    
          });
        //  this.isLoggedIn = true;
        }
}
