import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the SearchPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'searchCar',
}) 
export class SearchCarPipe implements PipeTransform {
  /**   
   * Takes a value and makes it lowercase.
   */ 
  transform(items: any[], terms: string): any[] {
    if(!items) return []; 
    if(!terms) return items;
   let terms2 = terms.replace(/\s+/g,'');
   function chunk(str, n) {
    var ret = [];
    var i;
    var len;

    for(i = 0, len = str.length; i < len; i += n) {
       ret.push(str.substr(i, n))
    }

    return ret
};


    return items.filter( it => {  
      return it.plate_no.includes(chunk(terms2,1).join(' '))||it.com_car_code.includes(terms); // only filter country name
    });
  }
}
