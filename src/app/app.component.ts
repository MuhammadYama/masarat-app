import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

// import { TabsPage } from '../pages/tabs/tabs';
// import { HomePage } from '../pages/home/home'; 
 import { LoginPage } from '../pages/login/login';
 import { ProfilePage } from '../pages/profile/profile'; 
// import { ClientTypePage } from '../pages/client-type/client-type';
// import { NewBillPage } from '../pages/new-bill/new-bill';
// import { ClientDetailPage } from '../pages/client-detail/client-detail';
import { AddServicesPage } from '../pages/add-services/add-services'; 
// import { BillDtailsPage } from '../pages/bill-dtails/bill-dtails';
// import { BillDetails2Page } from '../pages/bill-details2/bill-details2';
// import { PackagesPage } from '../pages/packages/packages';
// import { RegisterClientPage } from '../pages/register-client/register-client';
// import { SearchPlatePage } from '../pages/search-plate/search-plate';
// import { NewReportPage } from '../pages/new-report/new-report';
// import { DailyReportPage } from '../pages/daily-report/daily-report';
// import { MyReportsPage } from '../pages/my-reports/my-reports';
// import { ChatPage } from '../pages/chat/chat';
// import { ModalPage } from '../pages/modal/modal';
import { AuthenticatedUser } from '../models/user';
import { ApiServiceProvider } from '../providers/api-service/api-service';
import { StorageServiceProvider } from '../providers/storage-service/storage-service';

@Component({ 
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = LoginPage;
  rootPageParams :any;
  user: AuthenticatedUser;
  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen
     ,public userService: StorageServiceProvider
    , public apiServiceProvider:ApiServiceProvider) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });

    this.initializeApp();
  }


  initializeApp() {
   
this.userService.getOnStorage().then(
      (user) => {
        this.user=JSON.parse(user);
        if (!user || this.user.authdata==null){
             console.log('getAuthUser null '+JSON.stringify(user) ); 
          //this.navCtrl.push(LoginPage)
          this.rootPage=LoginPage;
        } else {
          console.log('getAuthUser1 '+JSON.stringify(user) );
          this.userService.getOnStorage('branch_id').then(
            (branch_id) => {
        
              if (branch_id){
                this.apiServiceProvider.setToken(this.user.authdata);
                //  this.rootPage=LoginPage;
               this.rootPageParams = { user: this.user ,branch_id:branch_id};
                this.rootPage=ProfilePage; 
              } /*else
            this.branch_id =this.user.branch['data']['id'];*/
        
          
        
            }
          );
           
        
    //  this.user = JSON.parse(user);

      //  this.items = this.user.data.visits;
        }

      }
    );

  }



}  