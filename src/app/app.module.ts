import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login';
import { ProfilePage } from '../pages/profile/profile';
import { ClientTypePage } from '../pages/client-type/client-type';
import { NewBillPage } from '../pages/new-bill/new-bill';
import { ClientDetailPage } from '../pages/client-detail/client-detail';
import { AddServicesPage } from '../pages/add-services/add-services';
import { AddServices2Page } from '../pages/add-services2/add-services2';

import { BillDtailsPage } from '../pages/bill-dtails/bill-dtails';
import { BillDetails2Page } from '../pages/bill-details2/bill-details2';
import { PackagesPage } from '../pages/packages/packages';
import { RegisterClientPage } from '../pages/register-client/register-client';
import { SearchPlatePage } from '../pages/search-plate/search-plate';
import { NewReportPage } from '../pages/new-report/new-report';
import { DailyReportPage } from '../pages/daily-report/daily-report';
import { MyReportsPage } from '../pages/my-reports/my-reports';
import { ChatPage } from '../pages/chat/chat';
import { ModalAddCarPage } from '../pages/modal-add-car/modal-add-car';
 

import { ModalPage } from '../pages/modal/modal';
 
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HttpClientModule } from '@angular/common/http';
import { ApiServiceProvider } from '../providers/api-service/api-service';

import { AuthServiceProvider } from '../providers/auth-service/auth-service'; 
import { StorageServiceProvider } from '../providers/storage-service/storage-service'; 
//import { NetworkProvider } from '../providers/network/network';
import { IonicStorageModule } from '@ionic/storage';
import { SearchPipe } from '../pipes/search/search';
import { SearchCarPipe } from '../pipes/search-car/search-car';
  
import { CompanyCarPage } from '../pages/company-car/company-car'; 
import { CompanyBillPage } from '../pages/company-bill/company-bill';
import { ReportHomePage } from '../pages/report-home/report-home';
import { ReportDetailsPage } from '../pages/report-details/report-details';
import { MyInvoicesPage } from '../pages/my-invoices/my-invoices';
import { InvoiceDetailesPage } from '../pages/invoice-detailes/invoice-detailes';
import { ReservationsPage } from '../pages/reservations/reservations';


@NgModule({ 
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    LoginPage,
    ProfilePage,
    ClientTypePage,
    NewBillPage,
    ClientDetailPage,
    AddServicesPage,
    BillDtailsPage,
    BillDetails2Page,
    PackagesPage,
    RegisterClientPage,
    SearchPlatePage,
    NewReportPage,
    DailyReportPage,
    MyReportsPage,
    ChatPage,
    ModalPage
    ,SearchPipe 
    ,SearchCarPipe
    ,ModalAddCarPage
    ,CompanyCarPage
    ,CompanyBillPage
    ,ReportHomePage
    ,ReportDetailsPage
    ,MyInvoicesPage
    ,InvoiceDetailesPage
    ,ReservationsPage
    ,AddServices2Page
  ],
  imports: [ 
    BrowserModule,
    IonicModule.forRoot(MyApp)
    ,  HttpClientModule 
    ,IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    LoginPage,
    ProfilePage,
    ClientTypePage,
    NewBillPage,
    ClientDetailPage,
    AddServicesPage,
    BillDtailsPage,
    BillDetails2Page,
    PackagesPage,
    RegisterClientPage,
    SearchPlatePage,
    NewReportPage,
    DailyReportPage,
    MyReportsPage,
    ChatPage,
    ModalAddCarPage
    , ModalPage
    ,CompanyCarPage 
    ,CompanyBillPage
    ,ReportHomePage
    ,ReportDetailsPage
    ,MyInvoicesPage
    ,InvoiceDetailesPage
    ,ReservationsPage
    ,AddServices2Page 
  ],
  providers: [
    StatusBar,
    SplashScreen, 
    {provide: ErrorHandler, useClass: IonicErrorHandler}
    , ApiServiceProvider
   
 
    // , NetworkProvider
    , StorageServiceProvider 
    , AuthServiceProvider
  ]

})
export class AppModule {}
    