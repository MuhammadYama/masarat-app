

export class Branch {

id: number;
name: string;
code: string;

constructor(obj?: any) {
    Object.assign(this, obj);
}
} 

/*
     {
            "id": 3,
            "company_id": 1,
            "name": "b3",
            "code": "b3",
            "created_at": "2019-01-29T20:22:55+00:00",
            "updated_at": "2019-01-29T20:22:55+00:00"
        }
*/