
export class Client {

    id: number;
    name: string;
    email: string;
    phone: string;
    address: string;

    constructor(obj?: any) {
        Object.assign(this, obj);
    }
    }
/*
    "id": 1,
    "company_id": 1,
    "user_id": null,
    "name": "عميل غير مسجل",
    "email": "customer@test.com",
    "tax_number": null,
    "phone": null,
    "address": null,
    "website": null,
    "currency_code": "ARS",
    "enabled": 1,
    "created_at": "2019-01-16T07:04:18+00:00",
    "updated_at": "2019-01-16T07:04:18+00:00"
    */